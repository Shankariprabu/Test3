package com.atlassian.jira.rpc.soap.beans;

public class RemoteNamedObject extends AbstractNamedRemoteEntity
{
    public RemoteNamedObject(String id, String name)
    {
        super(id, name);
    }
}
