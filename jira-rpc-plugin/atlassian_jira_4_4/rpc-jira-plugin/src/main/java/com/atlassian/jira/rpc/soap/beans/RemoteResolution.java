/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

/*
 */
package com.atlassian.jira.rpc.soap.beans;

import org.ofbiz.core.entity.GenericValue;

public class RemoteResolution extends AbstractRemoteConstant
{
    ///CLOVER:OFF
    public RemoteResolution()
    {
    }

    public RemoteResolution(GenericValue gv)
    {
        super(gv);
    }
}
