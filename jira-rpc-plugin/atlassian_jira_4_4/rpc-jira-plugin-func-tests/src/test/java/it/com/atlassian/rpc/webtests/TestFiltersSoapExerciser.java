package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteFilter;
import com.atlassian.jira.rpc.soap.client.RemoteIssue;

import java.rmi.RemoteException;

/**
 *
 */
public class TestFiltersSoapExerciser extends SOAPTestCase
{

    public static String FILTER_ID = "10000";

    public void testGetFilters() throws RemoteException
    {
        RemoteFilter[] filters = filtersSoapExerciser.testGetFilters();
        assertEquals(2, filters.length);
    }

    public void testGetAllIssues() throws RemoteException
    {
        RemoteIssue[] issues = filtersSoapExerciser.testGetIssuesFromFilter(FILTER_ID);
        assertEquals(1, issues.length);
        assertEquals(ISSUE_KEY, issues[0].getKey());
    }

    public void testGetIssueCountForFilter() throws RemoteException
    {
        long issueCount = filtersSoapExerciser.testGetIssueCountForFilter(FILTER_ID);
        assertEquals(1, issueCount);
    }

    public void testGetIssuesFromFilterWithLimit() throws Exception
    {
        restoreData("jira_soap_client_func_test_find_issues_with_limits.xml");
        soapConnect();

        final RemoteIssue[] issues = filtersSoapExerciser.testGetIssuesFromFilterWithLimit(FILTER_ID, 4, 2);
        assertEquals(2, issues.length);
    }

    public void testGetIssuesFromTextSearch() throws Exception
    {
        restoreData("jira_soap_client_func_test_find_issues_with_limits.xml");
        soapConnect();

        final RemoteIssue[] issues = filtersSoapExerciser.testGetIssuesFromTextSearch("bug");
        assertEquals(5, issues.length);
    }

    public void testGetIssuesFromTextSearchWithLimit() throws Exception
    {
        restoreData("jira_soap_client_func_test_find_issues_with_limits.xml");
        soapConnect();

        final RemoteIssue[] issues = filtersSoapExerciser.testGetIssuesFromTextSearchWithLimit("bug", 3, 1);
        assertEquals(1, issues.length);
    }

    public void testGetIssuesFromTextSearchWithProjectWithMax() throws Exception
    {
        restoreData("jira_soap_client_func_test_find_issues_with_limits.xml");
        soapConnect();

        final RemoteIssue[] issues = filtersSoapExerciser.testGetIssuesFromTextSearchWithProjectWithMax(new String [] {"HSP", "MKY"}, "task", 3);
        assertEquals(2, issues.length);
    }

    public void testGetIssuesFromTextSearchWithProjectWithMaxLimited() throws Exception
    {
        restoreData("jira_soap_client_func_test_find_issues_with_limits.xml");
        soapConnect();

        final RemoteIssue[] issues = filtersSoapExerciser.testGetIssuesFromTextSearchWithProjectWithMax(new String [] {"HSP", "MKY"}, "task", 1);
        assertEquals(1, issues.length);
    }
}