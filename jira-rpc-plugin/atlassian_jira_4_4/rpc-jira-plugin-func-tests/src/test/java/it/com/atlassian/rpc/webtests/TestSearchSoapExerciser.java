package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteIssue;
import org.apache.axis.AxisFault;


/**
 * @since v4.0
 */
public class TestSearchSoapExerciser extends SOAPTestCase
{
    public void testGetIssuesFromTextSearch() throws Exception
    {
        //With env field
        assertGetIssuesFromTextSearch(0, "NotPresent");
        assertGetIssuesFromTextSearch(1, "This is a new comment");

        restoreDataAndReconnect("jira_soap_client_func_test_hidden_environment.xml");

        //Without env field
        assertGetIssuesFromTextSearch(0, "NotPresent");
        assertGetIssuesFromTextSearch(1, "This is a new comment");
    }

    private void assertGetIssuesFromTextSearch(int expectedNumberOfIssues, String searchTerm) throws Exception
    {
        final RemoteIssue[] issues = searchSoapExerciser.testGetIssuesFromTextSearch(searchTerm);
        assertNotNull(issues);
        assertEquals(expectedNumberOfIssues, issues.length);
    }

    public void testGetIssuesFromTextSearchWithProject() throws Exception
    {
        //With env field
        assertGetIssuesFromTextSearchWithProject(0, new String[] {"HSP", "MKY"}, "NotPresent", 100);
        assertGetIssuesFromTextSearchWithProject(1, new String[] {"HSP", "MKY"}, "This is a new comment", 100);
        assertGetIssuesFromTextSearchWithProject(0, new String[] {"MKY"}, "This is a new comment", 100);

        restoreDataAndReconnect("jira_soap_client_func_test_hidden_environment.xml");

        //Without env field
        assertGetIssuesFromTextSearchWithProject(0, new String[] {"HSP", "MKY"}, "NotPresent", 100);
        assertGetIssuesFromTextSearchWithProject(1, new String[] {"HSP", "MKY"}, "This is a new comment", 100);
        assertGetIssuesFromTextSearchWithProject(0, new String[] {"MKY"}, "This is a new comment", 100);
    }

    private void assertGetIssuesFromTextSearchWithProject(int expectedNumberOfIssues, String[] projectKeys, String searchTerms, int maxNumResults) throws Exception
    {
        final RemoteIssue[] issues = searchSoapExerciser.testGetIssuesFromTextSearchWithProject(projectKeys, searchTerms, maxNumResults);
        assertNotNull(issues);
        assertEquals(expectedNumberOfIssues, issues.length);
    }


    public void testJql() throws Exception
    {
        restoreDataAndReconnect("TestQueriesInDocumentation.xml");

        assertSearchWithResults("project = \"New office\" and status = \"open\"", "NO-1");
        assertSearchWithResults("status = open and priority = urgent and assignee = jsmith", "ABC-3", "ABC-2");
        assertSearchWithResults("project = JRA and assignee != jsmith", "JRA-2");
        assertSearchWithResults("project in (JRA,CONF) and fixVersion = \"3.14\"", "JRA-2", "JRA-1", "CONF-1");
        assertSearchWithResults("reporter = jsmith or reporter = jbrown", "MKY-1", "HSP-1");
        assertSearchWithResults("duedate < now() or duedate is empty", "NO-1", "MKY-1", "JRA-2", "JRA-1", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("not assignee = jsmith", "NO-1", "MKY-1", "JRA-2", "HSP-1", "CONF-1", "ABC-4");
        assertSearchWithResults("assignee != jsmith", "NO-1", "MKY-1", "JRA-2", "HSP-1", "CONF-1", "ABC-4");
        assertSearchWithResults("reporter != jsmith", "NO-1", "MKY-1", "JRA-2", "JRA-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("reporter = currentUser() and assignee != currentUser()", "ABC-3", "ABC-2");
        assertSearchWithResults("assignee != \"John Smith\" or reporter != \"John Smith\"", "NO-1", "MKY-1", "JRA-2", "JRA-1", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("assignee != empty", "NO-1", "MKY-1", "JRA-2", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("assignee != empty", "NO-1", "MKY-1", "JRA-2", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("reporter in (jsmith,jbrown,jjones)", "MKY-1", "HSP-1", "ABC-4");
        assertSearchWithResults("reporter in (Jack,Jill) or assignee in (Jack,Jill)", "MKY-1");
        assertSearchWithResults("affectedVersion in (\"3.14\", \"4.2\")", "JRA-2", "JRA-1");
        assertSearchWithResults("reporter = jsmith", "HSP-1");
        assertSearchWithResults("reporter = \"John Smith\"", "HSP-1");
        assertSearchWithResults("votes > 4", "MKY-1");

        //These queries return the results always in the same order, because we store the created date down to the second.
        assertSearchWithResults("duedate = empty order by created", "ABC-4", "ABC-3", "HSP-1", "CONF-1", "JRA-2", "JRA-1", "ABC-2");
        assertSearchWithResults("duedate = empty order by created, priority desc", "ABC-4", "ABC-3", "HSP-1", "CONF-1", "JRA-2", "JRA-1", "ABC-2");
        assertSearchWithResults("duedate = empty order by created, priority asc", "ABC-4", "ABC-3", "HSP-1", "CONF-1", "JRA-2", "JRA-1", "ABC-2");

        assertSearchWithResults("summary ~ fin", "MKY-1");
        assertSearchWithResults("summary ~ Harry\\'s", "NO-1", "JRA-2");
        assertSearchWithResults("summary ~ Sales\\\\Marketing", "ABC-2", "ABC-3");
        assertSearchWithResults("summary ~ \"\\\"Harry's computer\\\"\"", "JRA-2");
        assertSearchWithResults("duedate = empty", "JRA-2", "JRA-1", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("duedate is empty", "JRA-2", "JRA-1", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("duedate = null", "JRA-2", "JRA-1", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("duedate is null", "JRA-2", "JRA-1", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");

        assertSearchWithResults("priority > normal", "MKY-1", "JRA-2", "JRA-1", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("votes >= 4", "MKY-1", "JRA-2");

        assertSearchWithResults("votes < 4", "NO-1", "JRA-1", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        assertSearchWithResults("votes <= 4", "NO-1", "JRA-2", "JRA-1", "HSP-1", "CONF-1", "ABC-4", "ABC-3", "ABC-2");

        assertSearchWithResults("duedate < now()  and resolution is empty", "NO-1", "MKY-1");
    }

    private void assertSearchWithResults(final String jql, final String... keys) throws Exception
    {
        final RemoteIssue[] issues = searchSoapExerciser.testJqlSearch(jql, 100);
        assertEquals(keys.length, issues.length);
        for (int i = 0; i < keys.length; i++)
        {
            String key = keys[i];
            assertEquals(key, issues[i].getKey());
        }
    }

    public void testLimiting() throws Exception
    {
        restoreDataAndReconnect("TestSearchService.xml");

        assertIssueKeys(searchSoapExerciser.testJqlSearch("issue is not empty", 3),
                new String[] { "MKY-2", "MKY-1", "HSP-4" });
    }

    public void testNullQuery() throws Exception
    {
        try
        {
            searchSoapExerciser.testJqlSearch(null);
            fail();
        }
        catch (AxisFault fault)
        {
            assertEquals("com.atlassian.jira.util.dbc.Assertions$NullArgumentException: query should not be null!", fault.getMessage());
        }
    }

    private void assertIssueKeys(final RemoteIssue[] issues, final String[] expectedKeys)
    {
        assertEquals(expectedKeys.length, issues.length);
        for (int i = 0; i < expectedKeys.length; i++)
        {
            String expectedKey = expectedKeys[i];
            assertEquals(expectedKey,  issues[i].getKey());
        }
    }

    // we won't attempt to be comprehensive here since we aren't testing the JQL parsing, just the RPC bridge aspect of it.
    public void testParseException() throws Exception
    {
        try
        {
            searchSoapExerciser.testJqlSearch("a bad query here");
            fail();
        }
        catch (AxisFault fault)
        {
            final String errorStart = "com.atlassian.jira.rpc.exception.RemoteValidationException: Parsing failed: Error in the JQL Query: Expecting operator but got 'bad'. The valid operators are '=', '!=', '<', '>', '<=', '>=', '~', '!~', 'IN', 'NOT IN', 'IS' and 'IS NOT'. (line 1, character 3)";
            assertEquals(fault.getFaultString().trim(), errorStart);
        }

        try
        {
            searchSoapExerciser.testJqlSearch("comment > 0");
            fail();
        }
        catch (AxisFault fault)
        {
            assertEquals("com.atlassian.jira.rpc.exception.RemoteValidationException: Query validation failed: The operator '>' is not supported by the 'comment' field.", fault.getFaultString().trim());
        }

        try
        {
            searchSoapExerciser.testJqlSearch("coment ~ *bad");
            fail();
        }
        catch (AxisFault fault)
        {
            final String errorMessage = "com.atlassian.jira.rpc.exception.RemoteValidationException: Parsing failed: Error in the JQL Query: The character '*' is a reserved JQL character. You must enclose it in a string or use the escape '\\u002a' instead. (line 1, character 10)";
            assertEquals(errorMessage, fault.getFaultString().trim());
        }

        try
        {
            searchSoapExerciser.testJqlSearch("cf[787823] is not empty");
            fail();
        }
        catch (AxisFault fault)
        {
            final String errorMessage = "com.atlassian.jira.rpc.exception.RemoteValidationException: Query validation failed: Field 'cf[787823]' does not exist or you do not have permission to view it.";
            assertEquals(errorMessage, fault.getFaultString().trim());
        }

        try
        {
            searchSoapExerciser.testJqlSearch("key in ()");
            fail();
        }
        catch (AxisFault fault)
        {
            final String errorMessage = "com.atlassian.jira.rpc.exception.RemoteValidationException: Parsing failed: Error in JQL Query: Expecting either a value, list or function but got ')'. You must surround ')' in quotation marks to use it as a value. (line 1, character 9)";
            assertEquals(errorMessage, fault.getFaultString().trim());
        }
    }
}
