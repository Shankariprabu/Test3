package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.*;
import com.atlassian.jira_soapclient.exercise.ExerciserClientConstants;
import com.google.common.collect.Sets;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Set;

/**
 *
 */
public class TestIssueSoapExerciser extends SOAPTestCase
{

    // Constant for get filter
    public static String SEARCH_TERM = "Test bug 1";

    public void testCreateIssue() throws RemoteException
    {
        final RemoteIssue returnedIssue = createIssue();
        //new issue shouldn't have a resolution date set.
        assertNull(issueSoapExerciser.testGetResolutionDate(returnedIssue.getKey()));
        assertNull(issueSoapExerciser.testGetResolutionDate(Long.valueOf(returnedIssue.getId())));
        gotoIssue(returnedIssue.getKey());
        assertTextPresent("This is a new SOAP issue ");

        //now resolve the issue and make sure we have a resolution date set.
        clickLink("action_id_5");
        setWorkingForm("issue-workflow-transition");
        submit("Transition");

        final RemoteIssue resolvedRemoteIssue = issueSoapExerciser.testGetIssueById(returnedIssue.getId());
        assertEquals("1", resolvedRemoteIssue.getResolution());
        assertNotNull(issueSoapExerciser.testGetResolutionDate(returnedIssue.getKey()));
        assertNotNull(issueSoapExerciser.testGetResolutionDate(Long.valueOf(returnedIssue.getId())));
    }

    // See JRA-25034
    public void testSelectAndMultiSelectAcceptsValuesAndOptions() throws RemoteException
    {
        final RemoteIssue returnedIssue = createIssue();

        assertCustomFieldValueIsPresent(returnedIssue, ExerciserClientConstants.CUSTOM_FIELD_KEY_4, Sets.<String>newHashSet("10011"));
        assertCustomFieldValueIsPresent(returnedIssue, ExerciserClientConstants.CUSTOM_FIELD_KEY_5, Sets.<String>newHashSet("10013", "10014", "10015"));

        // We can update using option values
        issueSoapExerciser.testUpdateIssue(returnedIssue.getKey(), ExerciserClientConstants.CUSTOM_FIELD_KEY_4, "apple", ExerciserClientConstants.CUSTOM_FIELD_KEY_5, "banana");

        final RemoteIssue updatedIssueUsingOptionValues = issueSoapExerciser.testGetIssueById(returnedIssue.getId());
        assertCustomFieldValueIsPresent(updatedIssueUsingOptionValues, ExerciserClientConstants.CUSTOM_FIELD_KEY_4, Sets.<String>newHashSet("10010"));
        assertCustomFieldValueIsPresent(updatedIssueUsingOptionValues, ExerciserClientConstants.CUSTOM_FIELD_KEY_5, Sets.<String>newHashSet("10013"));

        // And we can still update the usual, new way
        issueSoapExerciser.testUpdateIssue(returnedIssue.getKey(), ExerciserClientConstants.CUSTOM_FIELD_KEY_4, "10011", ExerciserClientConstants.CUSTOM_FIELD_KEY_5, "10014");

        final RemoteIssue updatedIssueUsingOptionIds = issueSoapExerciser.testGetIssueById(returnedIssue.getId());

        assertCustomFieldValueIsPresent(updatedIssueUsingOptionIds, ExerciserClientConstants.CUSTOM_FIELD_KEY_4, Sets.<String>newHashSet("10011"));
        assertCustomFieldValueIsPresent(updatedIssueUsingOptionIds, ExerciserClientConstants.CUSTOM_FIELD_KEY_5, Sets.<String>newHashSet("10014"));

        // In the case of a conflict, optionId takes precedence over option value
        issueSoapExerciser.testUpdateIssue(returnedIssue.getKey(), ExerciserClientConstants.CUSTOM_FIELD_KEY_4, "10010", ExerciserClientConstants.CUSTOM_FIELD_KEY_5, "10013");

        final RemoteIssue updatedIssueUsingConflictingOptionValues = issueSoapExerciser.testGetIssueById(returnedIssue.getId());

        assertCustomFieldValueIsPresent(updatedIssueUsingConflictingOptionValues, ExerciserClientConstants.CUSTOM_FIELD_KEY_4, Sets.<String>newHashSet("10010"));
        assertCustomFieldValueIsPresent(updatedIssueUsingConflictingOptionValues, ExerciserClientConstants.CUSTOM_FIELD_KEY_5, Sets.<String>newHashSet("10013"));

        // And if we can't figure it out...
        try
        {
            RemoteFieldValue[] actionParams = new RemoteFieldValue[] {
                new RemoteFieldValue(ExerciserClientConstants.CUSTOM_FIELD_KEY_5, new String[] { "10015", "banana" })
            };

            soapSession.getJiraSoapService().updateIssue(getToken(), returnedIssue.getKey(), actionParams);
            fail("Expected failure if a mix of Option Ids and Option Values are supplied to a MultiSelect custom field");
        }
        catch (RemoteException e)
        {
            // Success
        }

        // Garbage in gives garbage out still
        try
        {
            RemoteFieldValue[] actionParams = new RemoteFieldValue[] {
                new RemoteFieldValue(ExerciserClientConstants.CUSTOM_FIELD_KEY_5, new String[] { "durian" })
            };

            soapSession.getJiraSoapService().updateIssue(getToken(), returnedIssue.getKey(), actionParams);
            fail("Expected failure if supplied options cannot be matched to a value or id.");
        }
        catch (RemoteException e)
        {
            // Success
        }

    }

    private RemoteIssue createIssue() throws RemoteException
    {
        RemoteIssue issue = new RemoteIssue();
        issue.setProject(ExerciserClientConstants.PROJECT_KEY);
        issue.setType(ExerciserClientConstants.ISSUE_TYPE_ID);

        issue.setSummary(ExerciserClientConstants.SUMMARY_NAME);
        issue.setPriority(ExerciserClientConstants.PRIORITY_ID);
        issue.setDuedate(Calendar.getInstance());
        issue.setAssignee("");

        // Add remote compoments
        RemoteComponent component = new RemoteComponent();
        component.setId(ExerciserClientConstants.COMPONENT_ID);
        issue.setComponents(new RemoteComponent[] { component });

        // Add remote versions
        RemoteVersion version = new RemoteVersion();
        version.setId(ExerciserClientConstants.VERSION_ID);
        RemoteVersion[] remoteVersions = new RemoteVersion[] { version };
        issue.setFixVersions(remoteVersions);

        // Add custom fields
        RemoteCustomFieldValue customFieldValue = new RemoteCustomFieldValue(ExerciserClientConstants.CUSTOM_FIELD_KEY_1, "", new String[] { ExerciserClientConstants.CUSTOM_FIELD_VALUE_1 });
        RemoteCustomFieldValue customFieldValue2 = new RemoteCustomFieldValue(ExerciserClientConstants.CUSTOM_FIELD_KEY_2, "", new String[] { ExerciserClientConstants.CUSTOM_FIELD_VALUE_2 });

        // Add a cascading select custom field
        RemoteCustomFieldValue customFieldValue3 = new RemoteCustomFieldValue(ExerciserClientConstants.CUSTOM_FIELD_KEY_3, null, new String[] { ExerciserClientConstants.CUSTOM_FIELD_VALUE_3 });
        RemoteCustomFieldValue customFieldValue4 = new RemoteCustomFieldValue(ExerciserClientConstants.CUSTOM_FIELD_KEY_3, "1", new String[] { ExerciserClientConstants.CUSTOM_FIELD_VALUE_4 });

        // Add a select custom field
        RemoteCustomFieldValue customFieldValue5 = new RemoteCustomFieldValue(ExerciserClientConstants.CUSTOM_FIELD_KEY_4, null, new String[] { "orange" });

        // Add a multi-select custom field
        RemoteCustomFieldValue customFieldValue6 = new RemoteCustomFieldValue(ExerciserClientConstants.CUSTOM_FIELD_KEY_5, null, new String[] { "banana", "pear", "10013" });

        RemoteCustomFieldValue[] customFieldValues = new RemoteCustomFieldValue[] { customFieldValue, customFieldValue2, customFieldValue3, customFieldValue4, customFieldValue5, customFieldValue6 };
        issue.setCustomFieldValues(customFieldValues);

        final RemoteIssue returnedIssue = issueSoapExerciser.testCreateIssue(issue);
        return returnedIssue;
    }


    public void testFindIssuesWithTerm() throws RemoteException
    {
        RemoteIssue[] issues = issueSoapExerciser.testFindIssuesWithTerm(SEARCH_TERM);
        assertEquals(1, issues.length);
        assertEquals(ISSUE_KEY, issues[0].getKey());
    }


    public void testGetCustomFieldValues() throws RemoteException
    {
        RemoteCustomFieldValue[] customFieldValues = issueSoapExerciser.testGetCustomFieldValues(ISSUE_KEY);
        assertEquals(2, customFieldValues.length);
        for (int i = 0; i < customFieldValues.length; i++)
        {
            RemoteCustomFieldValue customFieldValue = customFieldValues[i];
            if (customFieldValue.getCustomfieldId().equals(ExerciserClientConstants.CUSTOM_FIELD_KEY_1))
            {
                assertEquals("custom field 1 value", customFieldValue.getValues()[0]);
            }
            else
            {
                assertEquals("custom field 2 value", customFieldValue.getValues()[0]);
            }
        }
    }

    public void testUpdateIssue() throws RemoteException
    {
        // First create the issue
        final RemoteIssue remoteIssue = createIssue();
        // Now update the issue and make sure that we did not over-write the existing field values
        testUpdateIssue(remoteIssue.getKey(), "customfield_10000", "text 1", "customfield_10001", "text 2", "new summary");
        // Make sure all the existing fields are still there.
        assertTextPresent("New Version 1");
        assertTextPresent("New Component 1");
    }


    public void testUpdateIssue(final String issueKey, final String custom_field_key_1, final String custom_field_value_1, final String custom_field_key_2, final String custom_field_value_2, final String issueSummary)
            throws RemoteException
    {
        issueSoapExerciser.testUpdateIssue(issueKey, custom_field_key_1, custom_field_value_1, custom_field_key_2, custom_field_value_2);
        gotoIssue(issueKey);
        assertTextPresent(custom_field_value_1);
        assertTextPresent(custom_field_value_2);
    }

    /*
     * JRA-13620: custom fields don't get populated with defaults
     *
     * TestCreateIssueWithDefaultCustomFieldValues.xml contains customfield_10020 called "ALIST" which is a Select List
     * and has 3 options: Face, Space, Place. The default is Face. When creating a remote issue without specifying
     * custom field values, Face should be populated as it is the default.
     */
    public void testDefaultValues() throws Exception
    {
        final String newFeatureType = "2";

        restoreDataAndReconnect("TestCreateIssueWithDefaultCustomFieldValues.xml");

        // only entering the bare essential fields - let everything else be set to default values
        RemoteIssue issue = new RemoteIssue();
        issue.setProject(ExerciserClientConstants.PROJECT_KEY);
        issue.setType(newFeatureType);
        issue.setSummary(ExerciserClientConstants.SUMMARY_NAME);

        final RemoteIssue returnedIssue = issueSoapExerciser.testCreateIssue(issue);

        assertEquals("admin", returnedIssue.getReporter()); // the user executing the createIssue request
        assertEquals("admin", returnedIssue.getAssignee()); // the default assignee for the workflow
        assertEquals(null, returnedIssue.getDescription());
        assertEquals(null, returnedIssue.getEnvironment());
        assertEquals("1", returnedIssue.getStatus()); // "Open"
        assertEquals("3", returnedIssue.getPriority()); // "Major" is the default when none is specified in the system
        assertEquals(null, returnedIssue.getResolution()); // "Unresolved" is apparently null
        assertCustomFieldValueIsPresent(returnedIssue, "customfield_10020", Collections.singleton("10011")); // We explicitly set a default for this custom field

        gotoIssue(returnedIssue.getKey());
        assertTextSequence(new String[] {"ALIST", "Face"});
    }

    /*
     * JRA-16498: versions should be validated correctly
     */
    public void testVersionValidation() throws Exception
    {
        // only entering the bare essential fields - let everything else be set to default values
        RemoteIssue issue = new RemoteIssue();
        issue.setProject(ExerciserClientConstants.PROJECT_KEY);
        issue.setType("2");
        issue.setSummary(ExerciserClientConstants.SUMMARY_NAME);

        // invalid version should throw Exception
        RemoteVersion version = new RemoteVersion();
        version.setId("1111");
        RemoteVersion[] remoteVersions = new RemoteVersion[] { version };
        issue.setFixVersions(remoteVersions);

        try
        {
            issueSoapExerciser.testCreateIssue(issue);
            fail("Expected RemoteValidationException for invalid version");
        }
        catch (RemoteValidationException expected) {}

        // valid version should be okay
        final String goodId = "10002";
        version.setId(goodId);
        final RemoteIssue remoteIssue = issueSoapExerciser.testCreateIssue(issue);
        assertEquals(1, remoteIssue.getFixVersions().length);
        assertEquals(goodId, remoteIssue.getFixVersions()[0].getId());
    }

    /*
     * JRA-16498: components should be validated correctly
     */
    public void testComponentValidation() throws Exception
    {
        // only entering the bare essential fields - let everything else be set to default values
        RemoteIssue issue = new RemoteIssue();
        issue.setProject(ExerciserClientConstants.PROJECT_KEY);
        issue.setType("2");
        issue.setSummary(ExerciserClientConstants.SUMMARY_NAME);

        // invalid version should throw Exception
        RemoteComponent component = new RemoteComponent();
        component.setId("1111");
        RemoteComponent[] remoteComponents = new RemoteComponent[] { component };
        issue.setComponents(remoteComponents);

        try
        {
            issueSoapExerciser.testCreateIssue(issue);
            fail("Expected RemoteValidationException for invalid component");
        }
        catch (RemoteValidationException expected) {}

        // valid version should be okay
        final String goodId = "10002";
        component.setId(goodId);
        final RemoteIssue remoteIssue = issueSoapExerciser.testCreateIssue(issue);
        assertEquals(1, remoteIssue.getComponents().length);
        assertEquals(goodId, remoteIssue.getComponents()[0].getId());
    }

    private void assertCustomFieldValueIsPresent(RemoteIssue remoteIssue, String customFieldId, Set<String> expectedValue)
    {
        final RemoteCustomFieldValue[] customFieldValues = remoteIssue.getCustomFieldValues();
        boolean found = false;
        for (int i = 0; i < customFieldValues.length; i++)
        {
            RemoteCustomFieldValue customFieldValue = customFieldValues[i];
            if (customFieldId.equals(customFieldValue.getCustomfieldId()))
            {
                assertEquals(expectedValue, Sets.newHashSet(customFieldValue.getValues()));
                found = true;
            }
        }
        assertTrue("Did not find a custom field value for '" + customFieldId + "'.", found);
    }
}