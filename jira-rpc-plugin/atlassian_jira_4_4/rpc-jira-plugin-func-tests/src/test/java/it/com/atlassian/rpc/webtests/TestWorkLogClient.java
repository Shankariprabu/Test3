package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.functest.framework.changehistory.ChangeHistoryList;
import com.atlassian.jira.functest.framework.changehistory.ChangeHistoryParser;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.rpc.soap.client.JiraSoapService;
import com.atlassian.jira.rpc.soap.client.RemoteConfiguration;
import com.atlassian.jira.rpc.soap.client.RemoteValidationException;
import com.atlassian.jira.rpc.soap.client.RemoteWorklog;
import com.atlassian.jira.webtests.Groups;
import com.atlassian.jira.webtests.Permissions;
import com.atlassian.jira_soapclient.exercise.ExerciserClientConstants;
import org.xml.sax.SAXException;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 * SOAP func test for the Worklog features of the SoapClient.
 */
public class TestWorkLogClient extends SOAPTestCase
{
    public static final String ISSUE_KEY = "HSP-1";

    private Map<String, RemoteWorklog> worklogMap;

    public TestWorkLogClient(String s)
    {
        super(s);
    }

    public void setUp()
    {
        super.setUp();
        worklogMap = new HashMap<String, RemoteWorklog>();
    }

    /**
     * This test takes a number of different invalid timeSpent formats and runs them against ALL of the the SOAP APIS
     * that can take them.  Its uses the nifty SoapCommand interface to mkae the code tidy.
     *
     * @throws RemoteException on remote errors
     */
    public void testAddWorklogNoTimeTracking() throws RemoteException
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        final String token = getToken();

        final String[] spentTimeValues = new String[] { "1s", "100", "0h", "0m", "0d", "" };
        for (String spentTimeValue : spentTimeValues)
        {
            final RemoteWorklog remoteWorklog = new RemoteWorklog();
            remoteWorklog.setId("12345");
            remoteWorklog.setTimeSpent(spentTimeValue);
            runWorklogMethodsExpectingValidationException(service, token, remoteWorklog);
        }
    }

    /*
     * Runs all the worklog methods expecting validation errors
     */
    private void runWorklogMethodsExpectingValidationException(final JiraSoapService service, final String token, final RemoteWorklog remoteWorklog)
    {
        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.addWorklogWithNewRemainingEstimate(token, ISSUE_KEY, remoteWorklog, "1d");
            }
        });
        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.updateWorklogWithNewRemainingEstimate(token, remoteWorklog, "1d");
            }
        });
        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.deleteWorklogWithNewRemainingEstimate(token, "37373", "1d");
            }
        });

        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.addWorklogAndAutoAdjustRemainingEstimate(token, ISSUE_KEY, remoteWorklog);
            }
        });
        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.updateWorklogAndAutoAdjustRemainingEstimate(token, remoteWorklog);
            }
        });
        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.deleteWorklogAndAutoAdjustRemainingEstimate(token, "37373");
            }
        });

        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.addWorklogAndRetainRemainingEstimate(token, ISSUE_KEY, remoteWorklog);
            }
        });
        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.updateWorklogAndRetainRemainingEstimate(token, remoteWorklog);
            }
        });
        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.deleteWorklogAndRetainRemainingEstimate(token, "37373");
            }
        });
    }

    public void testAddWorklogsWithinInvalidObjects()
    {
        activateTimeTracking();
        final JiraSoapService service = soapSession.getJiraSoapService();
        final String token = getToken();

        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                RemoteWorklog rwEmpty = new RemoteWorklog();
                service.addWorklogAndRetainRemainingEstimate(token, ISSUE_KEY, rwEmpty);
            }
        });

        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                RemoteWorklog rwMissingWhen = new RemoteWorklog();
                rwMissingWhen.setTimeSpent("1h");
                service.addWorklogAndRetainRemainingEstimate(token, ISSUE_KEY, rwMissingWhen);
            }
        });

        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                RemoteWorklog rwMissingTimeSpent = new RemoteWorklog();
                rwMissingTimeSpent.setStartDate(Calendar.getInstance());
                service.addWorklogAndRetainRemainingEstimate(token, ISSUE_KEY, rwMissingTimeSpent);
            }
        });

        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                RemoteWorklog rwBadTimeSpentFormat = new RemoteWorklog();
                rwBadTimeSpentFormat.setTimeSpent("100");
                service.addWorklogAndRetainRemainingEstimate(token, ISSUE_KEY, rwBadTimeSpentFormat);
            }
        });

        runExpectingRemoteValidationException(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                RemoteWorklog rwBadId = new RemoteWorklog();
                rwBadId.setId("123abc");
                service.updateWorklogAndRetainRemainingEstimate(token, rwBadId);
            }
        });
    }

    public void testAddMultipleNewEstimateWorklogs() throws RemoteException
    {
        activateTimeTracking();

        JiraSoapService service = soapSession.getJiraSoapService();
        String token = getToken();
        for (int i = 0; i < 10; i++)
        {
            RemoteWorklog remoteWorklog = getRemoteWorklog(i);
            service.addWorklogWithNewRemainingEstimate(token, ISSUE_KEY, remoteWorklog, "1d");
        }
        RemoteWorklog[] remoteWorklogs = service.getWorklogs(token, ISSUE_KEY);
        assertEquals(10, remoteWorklogs.length);
        for (int i = 0; i < remoteWorklogs.length; i++)
        {
            RemoteWorklog retrievedWorklog = remoteWorklogs[i];
            RemoteWorklog expected = worklogMap.get(String.valueOf(i));
            assertWorklogEquivalent(expected, retrievedWorklog);
        }
    }

    private void runExpectingRemoteValidationException(SoapCommand runMe)
    {
        try
        {
            runMe.execute();
            fail("time tracking not enabled, expected RemoteValidationException ");
        }
        catch (RemoteValidationException expected)
        {
            // expected
        }
        catch (RemoteException notQuite)
        {
            fail("expected RemoteValidationException");
        }
    }

    public void testWorklogAndEstimates() throws RemoteException, SAXException
    {
        activateTimeTracking();
        RemoteWorklog remoteWorklog = getRemoteWorklog();
        remoteWorklog.setTimeSpent("1h");

        JiraSoapService service = soapSession.getJiraSoapService();
        String token = getToken();

        service.addWorklogWithNewRemainingEstimate(token, ISSUE_KEY, remoteWorklog, "7d");
        RemoteWorklog[] remoteWorklogs = service.getWorklogs(token, ISSUE_KEY);
        assertEquals(1, remoteWorklogs.length);
        RemoteWorklog retrievedWorklog = remoteWorklogs[0];
        assertWorklogEquivalent(remoteWorklog, retrievedWorklog);

        assertTimeSpent(ISSUE_KEY, "1h");
        assertRemainingEstimate(ISSUE_KEY, "1w");

        remoteWorklog.setComment("second worklog for confirming new remaining estimate update");
        service.addWorklogWithNewRemainingEstimate(token, ISSUE_KEY, remoteWorklog, "3d");
        assertTimeSpent(ISSUE_KEY, "2h");
        assertRemainingEstimate(ISSUE_KEY, "3d");

        // NOW autoadjust
        remoteWorklog.setTimeSpent("1d");
        remoteWorklog.setComment("third worklog for confirming autoadjust remaining estimate");
        service.addWorklogAndAutoAdjustRemainingEstimate(token, ISSUE_KEY, remoteWorklog);
        assertTimeSpent(ISSUE_KEY, "1d 2h");
        assertRemainingEstimate(ISSUE_KEY, "2d");

        remoteWorklog.setComment("fourth worklog for confirming retain remaining estimate");
        service.addWorklogAndRetainRemainingEstimate(token, ISSUE_KEY, remoteWorklog);
        // remaining estimate should be unchanged.
        assertTimeSpent(ISSUE_KEY, "2d 2h");
        assertRemainingEstimate(ISSUE_KEY, "2d");

        // now check change history is right
        gotoIssue(ISSUE_KEY);
        clickLinkWithText("History");


        ChangeHistoryList expectedList = new ChangeHistoryList();

        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "1 hour [ 3600 ]")
                .add("Remaining Estimate", "1 week [ 604800 ]");


        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "2 hours [ 7200 ]")
                .add("Remaining Estimate", "3 days [ 259200 ]");


        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "1 day, 2 hours [ 93600 ] ")
                .add("Remaining Estimate", "2 days [ 172800 ]");

        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "2 days, 2 hours [ 180000 ] ");


        ChangeHistoryList actualList = ChangeHistoryParser.getChangeHistory(getTester());
        actualList.assertContainsSomeOf(expectedList);
    }

    public void testUpdateWorklogsWithNoEditPermission() throws RemoteException, SAXException
    {
        activateTimeTracking();
        final RemoteWorklog remoteWorklog = getRemoteWorklog();
        final JiraSoapService service = soapSession.getJiraSoapService();
        final String token = getToken();

        service.addWorklogWithNewRemainingEstimate(token, ISSUE_KEY, remoteWorklog, "7d");
        RemoteWorklog[] remoteWorklogs = service.getWorklogs(token, ISSUE_KEY);
        final RemoteWorklog retrievedWorklog = remoteWorklogs[0];


        retrievedWorklog.setComment("Comment - updateWorklogAndRetainRemainingEstimate");
        retrievedWorklog.setTimeSpent("1d");
        runExpectingPermissionFailure(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.updateWorklogAndRetainRemainingEstimate(token, retrievedWorklog);
            }
        });
        runExpectingPermissionFailure(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.updateWorklogAndAutoAdjustRemainingEstimate(token, retrievedWorklog);
            }
        });
        runExpectingPermissionFailure(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.updateWorklogWithNewRemainingEstimate(token, retrievedWorklog, "1d");
            }
        });
    }

    private void runExpectingPermissionFailure(SoapCommand runMe)
    {
        try
        {
            runMe.execute();
            fail("We shouldnt have permission to do this");
        }
        catch (RemoteValidationException expected)
        {
            // expected
            assertContains("you do not have permission to", expected.toString());
        }
        catch (RemoteException notQuite)
        {
            fail("expected RemoteValidationException");
        }
    }

    public void testUpdateWorklogs() throws RemoteException, SAXException
    {
        activateTimeTracking();
        grantPermissionToFixedUser(Permissions.WORKLOG_EDIT_ALL);

        JiraSoapService service = soapSession.getJiraSoapService();
        String token = getToken();

        RemoteWorklog startingWorklog = getRemoteWorklog();
        startingWorklog.setTimeSpent("1h");
        service.addWorklogWithNewRemainingEstimate(token, ISSUE_KEY, startingWorklog, "7d");

        RemoteWorklog[] remoteWorklogs = service.getWorklogs(token, ISSUE_KEY);
        RemoteWorklog remoteWorklog = remoteWorklogs[0];

        remoteWorklog.setComment("Comment - updateWorklogAndRetainRemainingEstimate");
        remoteWorklog.setTimeSpent("2d");

        service.updateWorklogAndRetainRemainingEstimate(token, remoteWorklog);

        // remaining estimate should be unchanged.
        assertRemainingEstimate(ISSUE_KEY, "1w");
        assertTimeSpent(ISSUE_KEY, "2d");

        // change the time spent from 2 days to 1 day and auto adjust the remaining estimate.
        // this will mean the remaining estimate should go up by the difference to 1 week, 1 day
        remoteWorklog.setComment("Comment - updateWorklogAndAutoAdjustRemainingEstimate");
        remoteWorklog.setTimeSpent("1d");
        service.updateWorklogAndAutoAdjustRemainingEstimate(token, remoteWorklog);
        assertRemainingEstimate(ISSUE_KEY, "1w 1d");

        remoteWorklog.setComment("Comment - updateWorklogWithNewRemainingEstimate");
        remoteWorklog.setTimeSpent("1d");
        service.updateWorklogWithNewRemainingEstimate(token, remoteWorklog, "14d");
        assertRemainingEstimate(ISSUE_KEY, "2w");

        gotoIssue(ISSUE_KEY);
        clickLinkWithText("History");

        ChangeHistoryList expectedList = new ChangeHistoryList();

        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "1 hour [ 3600 ]")
                .add("Remaining Estimate", "1 week [ 604800 ]");


        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "2 days [ 172800 ]");

        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "1 day [ 86400 ]")
                .add("Remaining Estimate", "1 week, 1 day [ 691200 ]");

        expectedList.addChangeSet("Administrator")
                .add("Remaining Estimate", "2 weeks [ 1209600 ]");

        ChangeHistoryList actualList = ChangeHistoryParser.getChangeHistory(getTester());
        actualList.assertContainsSomeOf(expectedList);
    }

    public void testDeleteWorklogs() throws RemoteException, SAXException
    {
        activateTimeTracking();
        grantPermissionToFixedUser(Permissions.WORKLOG_DELETE_ALL);

        JiraSoapService service = soapSession.getJiraSoapService();
        String token = getToken();

        // make 3 1hr worklogs with a remaining estimate of 3h
        RemoteWorklog worklog = getRemoteWorklog();
        worklog.setTimeSpent("1h");
        service.addWorklogWithNewRemainingEstimate(token, ISSUE_KEY, worklog, "3h");
        for (int i = 1; i <= 2; i++)
        {
            worklog.setComment("worklog numero " + i);
            service.addWorklogAndRetainRemainingEstimate(token, ISSUE_KEY, worklog);
        }

        RemoteWorklog[] remoteWorklogs = service.getWorklogs(token, ISSUE_KEY);
        assertEquals("should be 3 worklogs!!", 3, remoteWorklogs.length);

        assertRemainingEstimate(ISSUE_KEY, "3h");
        assertTimeSpent(ISSUE_KEY, "3h");

        service.deleteWorklogAndAutoAdjustRemainingEstimate(token, remoteWorklogs[0].getId());
        assertRemainingEstimate(ISSUE_KEY, "4h");
        assertTimeSpent(ISSUE_KEY, "2h");

        service.deleteWorklogWithNewRemainingEstimate(token, remoteWorklogs[1].getId(), "1h");
        assertRemainingEstimate(ISSUE_KEY, "1h");
        assertTimeSpent(ISSUE_KEY, "1h");

        service.deleteWorklogAndAutoAdjustRemainingEstimate(token, remoteWorklogs[2].getId());
        assertRemainingEstimate(ISSUE_KEY, "2h");
        assertTimeSpent(ISSUE_KEY, "0m"); // in the end we have done nothing!

        gotoIssue(ISSUE_KEY);
        clickLinkWithText("History");

        ChangeHistoryList expectedList = new ChangeHistoryList();

        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "1 hour [ 3600 ]")
                .add("Remaining Estimate", "3 hours [ 10800 ]");

        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "2 hours [ 7200 ]");

        expectedList.addChangeSet("Administrator")
                .add("Time Spent", "3 hours [ 10800 ]");

        expectedList.addChangeSet("Administrator")
                .add("Worklog Time Spent", "1 hour [ 3600 ]", "")
                .add("Worklog Id", "10000 [ 10000 ]", "")
                .add("Time Spent", "3 hours [ 10800 ]", "2 hours [ 7200 ]")
                .add("Remaining Estimate", "3 hours [ 10800 ]", "4 hours [ 14400 ]");

        expectedList.addChangeSet("Administrator")
                .add("Worklog Time Spent", "1 hour [ 3600 ]", "")
                .add("Worklog Id", "10001 [ 10001 ]", "")
                .add("Time Spent", "2 hours [ 7200 ]", "1 hour [ 3600 ]")
                .add("Remaining Estimate", "4 hours [ 14400 ]", "1 hour [ 3600 ]");

        expectedList.addChangeSet("Administrator")
                .add("Worklog Time Spent", "1 hour [ 3600 ]", "")
                .add("Worklog Id", "10002 [ 10002 ]", "")
                .add("Time Spent", "1 hour [ 3600 ]", "0 minutes [ 0 ]")
                .add("Remaining Estimate", "1 hour [ 3600 ]", "2 hours [ 7200 ]");

        ChangeHistoryList actualList = ChangeHistoryParser.getChangeHistory(getTester());
        actualList.assertContainsSomeOf(expectedList);
    }


    public void testDeleteWorklogsNoPermission() throws RemoteException
    {
        activateTimeTracking();
        final RemoteWorklog remoteWorklog = getRemoteWorklog();
        final JiraSoapService service = soapSession.getJiraSoapService();
        final String token = getToken();

        service.addWorklogWithNewRemainingEstimate(token, ISSUE_KEY, remoteWorklog, "7d");
        RemoteWorklog[] remoteWorklogs = service.getWorklogs(token, ISSUE_KEY);
        final RemoteWorklog retrievedWorklog = remoteWorklogs[0];

        runExpectingPermissionFailure(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.deleteWorklogAndRetainRemainingEstimate(token, retrievedWorklog.getId());
            }
        });
        runExpectingPermissionFailure(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.deleteWorklogAndAutoAdjustRemainingEstimate(token, retrievedWorklog.getId());
            }
        });
        runExpectingPermissionFailure(new SoapCommand()
        {
            public void execute() throws RemoteException
            {
                service.deleteWorklogWithNewRemainingEstimate(token, retrievedWorklog.getId(), "1d");
            }
        });
    }

    public void testTimeSpentFormating() throws RemoteException
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        final String token = getToken();
        RemoteWorklog rw;

        enableTTFormat("days");
        rw = addAndGetWorklog(service, token, "27h");
        assertEquals("1d 3h", rw.getTimeSpent());

        enableTTFormat("hours");
        rw = addAndGetWorklog(service, token, "27h");
        assertEquals("27h", rw.getTimeSpent());

        enableTTFormat("pretty");
        rw = addAndGetWorklog(service, token, "27h");
        assertEquals("1 day, 3 hours", rw.getTimeSpent());
    }

    private void enableTTFormat(String formatType)
    {
        activateTimeTracking();
        clickLink("timetracking");
        submit("Deactivate");
        checkCheckbox("timeTrackingFormat", formatType);
        submit("Activate");
    }

    public void testTimeTrackingEnabled() throws RemoteException
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        final String token = getToken();
        RemoteConfiguration rc = service.getConfiguration(token);
        assertFalse(rc.isAllowTimeTracking());

        activateTimeTracking();
        rc = service.getConfiguration(token);
        assertTrue(rc.isAllowTimeTracking());

        int hoursInDay = rc.getTimeTrackingHoursPerDay();
        int daysPerWeek = rc.getTimeTrackingDaysPerWeek();

        assertEquals(24, hoursInDay);
        assertEquals(7, daysPerWeek);

        clickLink("timetracking");
        submit("Deactivate");
        setFormElement("hoursPerDay", "15");
        setFormElement("daysPerWeek", "4");
        submit("Activate");

        rc = service.getConfiguration(token);
        assertTrue(rc.isAllowTimeTracking());

        hoursInDay = rc.getTimeTrackingHoursPerDay();
        daysPerWeek = rc.getTimeTrackingDaysPerWeek();

        assertEquals(15, hoursInDay);
        assertEquals(4, daysPerWeek);
    }

    public void testPermissions() throws RemoteException
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        final String token = getToken();
        RemoteConfiguration rc = service.getConfiguration(token);
        assertFalse(rc.isAllowTimeTracking());

        assertFalse(service.hasPermissionToCreateWorklog(token, ISSUE_KEY));

        activateTimeTracking();
        rc = service.getConfiguration(token);
        assertTrue(rc.isAllowTimeTracking());

        assertTrue(service.hasPermissionToCreateWorklog(token, ISSUE_KEY));

        // we dont have delete or edit by default in this XML dataset
        RemoteWorklog rw = addAndGetWorklog(service, token);
        String rwId = rw.getId();

        assertFalse(service.hasPermissionToDeleteWorklog(token, rwId));

        assertFalse(service.hasPermissionToUpdateWorklog(token, rwId));

        // give ourselves permission
        grantPermissionToFixedUser(Permissions.WORKLOG_DELETE_ALL);
        assertTrue(service.hasPermissionToDeleteWorklog(token, rwId));

        grantPermissionToFixedUser(Permissions.WORKLOG_EDIT_ALL);
        assertTrue(service.hasPermissionToUpdateWorklog(token, rwId));
    }

    /**
     * Test that the addWorklog* methods return the created remote worklog object - JRA-13260
     *
     * @throws java.rmi.RemoteException on remote errors
     */
    public void testAddWorklogAndGettingTheWorklogInResponse() throws RemoteException
    {
        activateTimeTracking();

        //check that initially the issue has no worklogs
        verifyCurrentWorklogs(new RemoteWorklog[] { });
        RemoteWorklog remoteWorklog1 = createRemoteWorklog("addWorklogWithNewRemainingEstimate", "1d", Groups.ADMINISTRATORS);
        RemoteWorklog createdWorklog1 = timeTrackingSoapExerciser.testAddWorklogWithNewRemainingEstimate(ISSUE_KEY, remoteWorklog1, "1w");
        verifyCreatedWorklog(ISSUE_KEY, "10000", remoteWorklog1, createdWorklog1, "1 day");
        verifyCurrentWorklogs(new RemoteWorklog[] { createdWorklog1 });

        RemoteWorklog remoteWorklog2 = createRemoteWorklog("AddWorklogAndAutoAdjustRemainingEstimate", "60h", Groups.DEVELOPERS);
        RemoteWorklog createdWorklog2 = timeTrackingSoapExerciser.testAddWorklogAndAutoAdjustRemainingEstimate(ISSUE_KEY, remoteWorklog2);
        verifyCreatedWorklog(ISSUE_KEY, "10001", remoteWorklog2, createdWorklog2, "2 days, 12 hours");
        verifyCurrentWorklogs(new RemoteWorklog[] { createdWorklog1, createdWorklog2 });

        RemoteWorklog remoteWorklog3 = createRemoteWorklog("AddWorklogAndRetainRemainingEstimate", "1h 2d 3w", null);
        RemoteWorklog createdWorklog3 = timeTrackingSoapExerciser.testAddWorklogAndRetainRemainingEstimate(ISSUE_KEY, remoteWorklog3);
        verifyCreatedWorklog(ISSUE_KEY, "10002", remoteWorklog3, createdWorklog3, "3 weeks, 2 days, 1 hour");
        verifyCurrentWorklogs(new RemoteWorklog[] { createdWorklog1, createdWorklog2, createdWorklog3 });
    }

    /*
        =================================================================
                   HELPER METHODS
        =================================================================
    */

    private RemoteWorklog createRemoteWorklog(String comment, String timeSpent, String groupLevel)
    {
        RemoteWorklog remoteWorklog = new RemoteWorklog();
//        remoteWorklog.setAuthor("fred"); //todo - author is ignored
        remoteWorklog.setComment(comment);
        remoteWorklog.setGroupLevel(groupLevel);
        remoteWorklog.setStartDate(Calendar.getInstance());
        remoteWorklog.setTimeSpent(timeSpent);
        return remoteWorklog;
    }

    private void verifyCreatedWorklog(String issueKey, String worklogId, RemoteWorklog remoteWorklog, RemoteWorklog createdWorklog, String timeSpent)
    {
        assertNotNull(createdWorklog);
        assertEquals(worklogId, createdWorklog.getId());
        assertEquals(remoteWorklog.getComment(), createdWorklog.getComment());
        assertEquals(remoteWorklog.getGroupLevel(), createdWorklog.getGroupLevel());
//        assertEquals(remoteWorklog.getTimeSpent(), createdWorklog.getTimeSpent());
//        assertEquals(remoteWorklog.getAuthor(), createdWorklog.getAuthor());
//        assertEquals(remoteWorklog.getCreated(), createdWorklog.getCreated());
//        assertEquals(remoteWorklog.getStartDate(), createdWorklog.getStartDate());

        //check that the worklog is actually shown up on the view issue page
        gotoIssueTabPanel(issueKey, ISSUE_TAB_WORK_LOG);
        assertTextPresent("/browse/" + issueKey + "?focusedWorklogId=" + worklogId + "&page=com.atlassian.jira.plugin.system.issuetabpanels%3Aworklog-tabpanel#worklog-" + worklogId);
        text.assertTextSequence(locator.id("worklog_details_" + worklogId), "Time Spent", timeSpent, createdWorklog.getComment());
    }

    private void verifyCurrentWorklogs(RemoteWorklog[] expectedWorklogs) throws RemoteException
    {
        RemoteWorklog[] currentWorklogs = timeTrackingSoapExerciser.testGetWorklogs(ISSUE_KEY);
        assertEquals(expectedWorklogs.length, currentWorklogs.length);

        //this is used because the worklogs returned by getWorklogs have slightly
        //different values and the RemoteWorklog.equals does not handle it.
        for (RemoteWorklog expectedWorklog : expectedWorklogs)
        {
            boolean valid = false;
            for (RemoteWorklog currentWorklog : currentWorklogs)
            {
                if (expectedWorklog.getId().equals(currentWorklog.getId()))
                {
                    assertWorklogEquivalent(expectedWorklog, currentWorklog);
                    valid = true;
                }
            }
            if (!valid)
            {
                fail("Expected worklog with id: '" + expectedWorklog.getId() + "' not found");
            }
        }
    }

    private RemoteWorklog addAndGetWorklog(JiraSoapService service, String token) throws RemoteException
    {
        return addAndGetWorklog(service, token, "1h");
    }

    private RemoteWorklog addAndGetWorklog(JiraSoapService service, String token, String timeSpent)
            throws RemoteException
    {
        RemoteWorklog worklog = getRemoteWorklog();
        worklog.setTimeSpent(timeSpent);
        service.addWorklogAndRetainRemainingEstimate(token, ISSUE_KEY, worklog);

        RemoteWorklog[] remoteWorklogs = service.getWorklogs(token, ISSUE_KEY);
        return remoteWorklogs[remoteWorklogs.length - 1];
    }

    /**
     * Asserts that the remaining estimate for the given issue contains the given text.
     * <p/>
     * TODO: Refactor into Func Test Framework
     *
     * @param issueKey the key of the isssue.
     * @param remainingEstimate the text to assert exists.
     * @throws SAXException if something freaky happens.
     */
    private void assertRemainingEstimate(String issueKey, String remainingEstimate)
            throws SAXException
    {
        gotoIssue(issueKey);
        text.assertTextPresent(new IdLocator(tester, "tt_single_values_remain"), remainingEstimate);
    }

    private void assertTimeSpent(String issueKey, String timeSpent)
            throws SAXException
    {
        gotoIssue(issueKey);
        text.assertTextPresent(new IdLocator(tester, "tt_single_values_spent"), timeSpent);
    }

    private void assertContains(String expectedSubstring, String stringToSearch)
    {
        if (!stringToSearch.contains(expectedSubstring))
        {
            fail("did not find '" + expectedSubstring + "' in '" + stringToSearch + "'");
        }
    }

    private void assertWorklogEquivalent(RemoteWorklog expectedWorklog, RemoteWorklog retrievedWorklog)
    {
        assertNotNull(retrievedWorklog);
        assertNotNull(expectedWorklog);
        assertNotNull(retrievedWorklog.getId());
        assertEquals(ExerciserClientConstants.LOGIN_NAME, retrievedWorklog.getAuthor());
        assertEquals(expectedWorklog.getComment(), retrievedWorklog.getComment());
        assertEquals(expectedWorklog.getGroupLevel(), retrievedWorklog.getGroupLevel());
        assertEquals(expectedWorklog.getRoleLevelId(), retrievedWorklog.getRoleLevelId());

        assertEquals(ExerciserClientConstants.LOGIN_NAME, retrievedWorklog.getUpdateAuthor());

        long diff = retrievedWorklog.getStartDate().getTime().getTime() - retrievedWorklog.getStartDate().getTime().getTime();
        int absoluteDifference = Math.abs((int) diff);
        assertLessThan(absoluteDifference, 1000);

    }

    private RemoteWorklog getRemoteWorklog()
    {
        return getRemoteWorklog(30);
    }

    private RemoteWorklog getRemoteWorklog(int index)
    {
        Calendar now = Calendar.getInstance();

        RemoteWorklog remoteWorklog = new RemoteWorklog();
        remoteWorklog.setComment("comment as of : " + now.toString());

        now.roll(Calendar.HOUR, -1);
        remoteWorklog.setStartDate(now);
        remoteWorklog.setTimeSpent(index + 1 + "m");

        worklogMap.put(String.valueOf(index), remoteWorklog);
        return remoteWorklog;
    }

    private void grantPermissionToFixedUser(int permissionCode)
    {
        grantPermissionToUserInEnterprise(permissionCode, ExerciserClientConstants.LOGIN_NAME);
    }

    /**
     * Functor abstraction for executing an operation which may throw a RemoteException. Intended for use in testing
     * SOAP methods where the invocation context is the same for a number of similar remote invocations.
     */
    interface SoapCommand
    {
        /**
         * The command call.
         *
         * @throws RemoteException when the SOAP service does.
         */
        void execute() throws RemoteException;
    }

}
