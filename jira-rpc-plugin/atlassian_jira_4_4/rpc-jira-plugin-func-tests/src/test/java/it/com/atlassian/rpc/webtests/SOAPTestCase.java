package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.functest.framework.FuncTestHelperFactory;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.webtests.JIRAWebTest;
import com.atlassian.jira_soapclient.SOAPSession;
import com.atlassian.jira_soapclient.exercise.AttachmentsSoapExerciser;
import com.atlassian.jira_soapclient.exercise.CommentsSoapExerciser;
import com.atlassian.jira_soapclient.exercise.CustomFieldsSoapExerciser;
import com.atlassian.jira_soapclient.exercise.FiltersSoapExerciser;
import com.atlassian.jira_soapclient.exercise.IssueSoapExerciser;
import com.atlassian.jira_soapclient.exercise.JiraAdminSoapExerciser;
import com.atlassian.jira_soapclient.exercise.ProjectAdminSoapExerciser;
import com.atlassian.jira_soapclient.exercise.SearchSoapExerciser;
import com.atlassian.jira_soapclient.exercise.TimeTrackingSoapExerciser;
import com.atlassian.jira_soapclient.exercise.UserAdminSoapExerciser;
import com.atlassian.jira_soapclient.exercise.WorkflowSoapExerciser;

import java.net.URL;
import java.rmi.RemoteException;

/**
 * A base class to derived func tests for SOAP APIS.  Based on the JIRA func test API
 * <p/>
 * NOTE : This code uses the localtest.properties to determine the JIRA server to connect to from a SOAP
 * point of view.  The old way assumed that the server that the WSDL was genrated from was the server to
 * communicate with.  While this is MOST likely still the case, it doesnt have to be.
 * <p/>
 * Its now technically possible to connect a JIRA instance other than the one the AXIS generated classes
 * where produced against.  Just change your localtest.properties.
 * <p/>
 * For example you could use this to run tests against a branch version of JIRA as well as a trunk version of JIRA
 * but using the saame WSDL structure (typically branches one)
 */
public abstract class SOAPTestCase extends JIRAWebTest
{
    protected static String LOGIN_NAME = "admin";
    protected static String LOGIN_PASSWORD = "admin";

    protected static final String ISSUE_ID = "10000";
    protected static final String ISSUE_KEY = "HSP-1";
    protected static final Long PROJECT_ID = 10000L;
    protected static final String PROJECT_KEY = "HSP";


    protected SOAPSession soapSession;
    protected AttachmentsSoapExerciser attachmentsSoapExerciser;
    protected CommentsSoapExerciser commentsSoapExerciser;
    protected CustomFieldsSoapExerciser customFieldsSoapExerciser;
    protected FiltersSoapExerciser filtersSoapExerciser;
    protected IssueSoapExerciser issueSoapExerciser;
    protected JiraAdminSoapExerciser jiraAdminSoapExerciser;
    protected ProjectAdminSoapExerciser projectAdminSoapExerciser;
    protected TimeTrackingSoapExerciser timeTrackingSoapExerciser;
    protected UserAdminSoapExerciser userAdminSoapExerciser;
    protected WorkflowSoapExerciser workflowSoapExerciser;
    protected SearchSoapExerciser searchSoapExerciser;

    protected Navigation navigation;

    public SOAPTestCase()
    {
        this("SOAP Test");
    }

    public SOAPTestCase(String s)
    {
        super(s);
    }

    public void setUp()
    {
        super.setUp();

        FuncTestHelperFactory factory = new FuncTestHelperFactory(this, getEnvironmentData());
        navigation = factory.getNavigation();

        restoreData("jira_soap_client_func_test.xml");
        try
        {
            String baseUrl = getServiceURL();
            soapSession = new SOAPSession(new URL(baseUrl));

            //
            // setup the exerciser objects to help us exercise the SOAP API
            attachmentsSoapExerciser = new AttachmentsSoapExerciser(soapSession);
            commentsSoapExerciser = new CommentsSoapExerciser(soapSession);
            customFieldsSoapExerciser = new CustomFieldsSoapExerciser(soapSession);
            filtersSoapExerciser = new FiltersSoapExerciser(soapSession);
            issueSoapExerciser = new IssueSoapExerciser(soapSession);
            jiraAdminSoapExerciser = new JiraAdminSoapExerciser(soapSession);
            projectAdminSoapExerciser = new ProjectAdminSoapExerciser(soapSession);
            timeTrackingSoapExerciser = new TimeTrackingSoapExerciser(soapSession);
            userAdminSoapExerciser = new UserAdminSoapExerciser(soapSession);
            workflowSoapExerciser = new WorkflowSoapExerciser(soapSession);
            searchSoapExerciser = new SearchSoapExerciser(soapSession);

            soapConnect();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            log("Unable to create the soapSession.", e);
            throw new RuntimeException(e);
        }
    }

    protected String getServiceURL()
    {
        final URL base = getEnvironmentData().getBaseUrl();
        return base.toString() + "/rpc/soap/jirasoapservice-v2";
    }

    public void soapConnect() throws RemoteException
    {
        soapConnect(LOGIN_NAME, LOGIN_PASSWORD);
    }

    public void soapConnect(String userName, String password) throws RemoteException
    {
        System.out.println("Connnecting via SOAP as : " + userName);
        soapSession.connect(userName, password);
    }

    public String getToken()
    {
        return soapSession.getAuthenticationToken();
    }

    protected void restoreDataAndReconnect(String fileName)
    {
        restoreData(fileName);
        try
        {
            soapConnect();
        }
        catch (RemoteException e)
        {
            throw new RuntimeException(e);
        }
    }
}