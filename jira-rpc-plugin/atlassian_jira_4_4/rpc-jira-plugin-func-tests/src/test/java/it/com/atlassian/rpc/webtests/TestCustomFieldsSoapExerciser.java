package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteField;

import java.rmi.RemoteException;

/**
 *
 */
public class TestCustomFieldsSoapExerciser extends SOAPTestCase
{

    public void testGetCustomFields() throws RemoteException
    {
        RemoteField[] fields = customFieldsSoapExerciser.testGetCustomFields();
        assertEquals(5, fields.length);
    }
}