package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteFieldValue;
import com.atlassian.jira.util.collect.CollectionBuilder;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class TestWorkflowSoapExerciser extends SOAPTestCase
{
    // this map is used to make assertions about the values in the top left issue detail section
    private final static Map<String, Object> ISSUE_DETAILS_FIELD_VALUE_MAP = new LinkedHashMap<String, Object>();
    // this map contains expected field values for all the fields shown on HSP-2
    private final static Map<String, Object> FIELD_VALUE_MAP = new LinkedHashMap<String, Object>();
    // this map is used to update each field to a new value one at a time
    private final static Map<String, FieldDetail> FIELD_UPDATE_MAP = new LinkedHashMap<String, FieldDetail>();

    static
    {
        ISSUE_DETAILS_FIELD_VALUE_MAP.put("Key", "HSP-2");
        ISSUE_DETAILS_FIELD_VALUE_MAP.put("Type", "Bug");
        ISSUE_DETAILS_FIELD_VALUE_MAP.put("Status", "Resolved");
        ISSUE_DETAILS_FIELD_VALUE_MAP.put("Priority", "Minor");
        ISSUE_DETAILS_FIELD_VALUE_MAP.put("Resolution", "Fixed");
        ISSUE_DETAILS_FIELD_VALUE_MAP.put("Assignee", "Administrator");
        ISSUE_DETAILS_FIELD_VALUE_MAP.put("Reporter", "Administrator");

        //summary doesn't have a label
        FIELD_VALUE_MAP.put("Values in all fields", null);
        FIELD_VALUE_MAP.put("Affects Version/s:", CollectionBuilder.newBuilder("New Version 1", "New Version 4").asList());
        FIELD_VALUE_MAP.put("Fix Version/s:", CollectionBuilder.newBuilder("New Version 1", "New Version 4").asList());
        FIELD_VALUE_MAP.put("Component/s:", "New Component 1");
        FIELD_VALUE_MAP.put("Environment", "My environment is MacOs");
        FIELD_VALUE_MAP.put("Free Text CF:", "This is some free text");
        FIELD_VALUE_MAP.put("Multi Checkbox:", "red");
        FIELD_VALUE_MAP.put("Multi select:", "monkey");
        FIELD_VALUE_MAP.put("Number CF:", "123,123");
        FIELD_VALUE_MAP.put("Project picker CF:", "homosapien");
        FIELD_VALUE_MAP.put("Radio Buttons CF:", "TV");
        FIELD_VALUE_MAP.put("Select CF:", "Nokia");
        FIELD_VALUE_MAP.put("Single Version CF:", "New Version 1");
        FIELD_VALUE_MAP.put("URL CF:", "http://www.msn.com/");
        FIELD_VALUE_MAP.put("Text CF:", "The sky is blue");
        FIELD_VALUE_MAP.put("Version CF:", "New Version 1");
        FIELD_VALUE_MAP.put("cascadingSelect:", CollectionBuilder.newBuilder("Fruit", "banana").asList());
        FIELD_VALUE_MAP.put("Description", "This is a really boring issue with an even lamer description.");
        //comment also doesn't have a label
        FIELD_VALUE_MAP.put("Yay! all fields have a value!", null);

        //User elements in the peoples block.
        FIELD_VALUE_MAP.put("Group Picker:", "jira-administrators");
        FIELD_VALUE_MAP.put("Multi Group Picker CF:", CollectionBuilder.newBuilder("jira-administrators", "jira-developers").asList());
        FIELD_VALUE_MAP.put("Multi User CF:", CollectionBuilder.newBuilder("Administrator", "Fred Normal").asList());
        FIELD_VALUE_MAP.put("user picker CF:", "Administrator");

        //Date elements in the dates block.
        FIELD_VALUE_MAP.put("Date picker cf:", "07/Apr/09");
        FIELD_VALUE_MAP.put("Date time:", "29/Apr/09 3:01 PM");

        FIELD_UPDATE_MAP.put("resolution", new FieldDetail("Resolution", "2", "Won&#39;t Fix", true));
        FIELD_UPDATE_MAP.put("fixVersions", new FieldDetail("Fix Version/s:", "10002", "New Version 5", false));
        FIELD_UPDATE_MAP.put("customfield_10000", new FieldDetail("cascadingSelect:", new String[] { "10012", "10016" }, new String[] { "Vegetable", "carrot" }, false, "customfield_10000:1"));
        FIELD_UPDATE_MAP.put("customfield_10001", new FieldDetail("Date time:", "14/Apr/09 1:01 PM", "14/Apr/09 1:01 PM", false));
        FIELD_UPDATE_MAP.put("customfield_10002", new FieldDetail("Group Picker:", "jira-users", "jira-users", false));
        FIELD_UPDATE_MAP.put("customfield_10003", new FieldDetail("Multi Checkbox:", "10001", "blue", false));
        FIELD_UPDATE_MAP.put("customfield_10004", new FieldDetail("Multi select:", "10004", "cat", false));
        FIELD_UPDATE_MAP.put("customfield_10005", new FieldDetail("Number CF:", "344", "344", false));
        FIELD_UPDATE_MAP.put("customfield_10006", new FieldDetail("Radio Buttons CF:", "10005", "Radio", false));
        FIELD_UPDATE_MAP.put("customfield_10007", new FieldDetail("Select CF:", "10008", "Apple", false));
        FIELD_UPDATE_MAP.put("customfield_10008", new FieldDetail("Text CF:", "The sea is blue too", "The sea is blue too", false));
        FIELD_UPDATE_MAP.put("customfield_10009", new FieldDetail("user picker CF:", "fred", "fred", false));
        FIELD_UPDATE_MAP.put("customfield_10010", new FieldDetail("Date picker cf:", "1/Apr/09", "1/Apr/09", false));
        FIELD_UPDATE_MAP.put("customfield_10011", new FieldDetail("Free Text CF:", "Lorem ipsum", "Lorem ipsum", false));
        FIELD_UPDATE_MAP.put("customfield_10013", new FieldDetail("Multi Group Picker CF:", "jira-users", "jira-users", false));
        FIELD_UPDATE_MAP.put("customfield_10014", new FieldDetail("Multi User CF:", "fred", "fred", false));
        FIELD_UPDATE_MAP.put("customfield_10015", new FieldDetail("Project picker CF:", "10001", "monkey", false));
        FIELD_UPDATE_MAP.put("customfield_10017", new FieldDetail("Single Version CF:", "10001", "New Version 4", false));
        FIELD_UPDATE_MAP.put("customfield_10018", new FieldDetail("URL CF:", "http://www.google.com", "http://www.google.com", false));
        FIELD_UPDATE_MAP.put("customfield_10019", new FieldDetail("Version CF:", "10002", "New Version 5", false));
        FIELD_UPDATE_MAP.put("versions", new FieldDetail("Affects Version/s:", "10002", "New Version 5", false));
        FIELD_UPDATE_MAP.put("components", new FieldDetail("Component/s:", "10002", "New Component 3", false));
        FIELD_UPDATE_MAP.put("description", new FieldDetail("Description", "A better description.", "A better description.", false));
        FIELD_UPDATE_MAP.put("environment", new FieldDetail("Environment", "Actually the environment is windows", "Actually the environment is windows", false));
        FIELD_UPDATE_MAP.put("priority", new FieldDetail("Priority", "2", "Critical", true));
    }

    private static class FieldDetail
    {
        private String fieldName;
        private String[] fieldUpdateValue;
        private String[] fieldUpdateName;
        private boolean isIssueDetail;
        private String optionalId;

        private FieldDetail(final String fieldName, final String fieldUpdateValue, final String fieldUpdateName, final boolean issueDetail)
        {
            this.fieldName = fieldName;
            this.fieldUpdateValue = new String[] { fieldUpdateValue };
            this.fieldUpdateName = new String[] { fieldUpdateName };
            isIssueDetail = issueDetail;
        }

        private FieldDetail(final String fieldName, final String[] fieldUpdateValue, final String[] fieldUpdateName, final boolean issueDetail, final String optionalId)
        {
            this.fieldName = fieldName;
            this.fieldUpdateValue = fieldUpdateValue;
            this.fieldUpdateName = fieldUpdateName;
            isIssueDetail = issueDetail;
            this.optionalId = optionalId;
        }

        public String getFieldName()
        {
            return fieldName;
        }

        public String[] getFieldUpdateValue()
        {
            return fieldUpdateValue;
        }

        public boolean isIssueDetail()
        {
            return isIssueDetail;
        }

        public String[] getFieldUpdateName()
        {
            return fieldUpdateName;
        }

        public String getOptionalId()
        {
            return optionalId;
        }

        @Override
        public String toString()
        {
            return "{fieldUpdateValue=" + (fieldUpdateValue == null ? null : Arrays.asList(fieldUpdateValue)) +
                    ", fieldUpdateName=" + (fieldUpdateName == null ? null : Arrays.asList(fieldUpdateName)) +
                    '}';
        }
    }

    public void testProgressWorkflow() throws RemoteException
    {
        workflowSoapExerciser.testProgressWorkflow(ISSUE_KEY);
        gotoIssue(ISSUE_KEY);
        assertTextPresent("In Progress");
        assertTextPresent("Stop Progress");
    }

    public void testResolveIssue() throws RemoteException
    {
        workflowSoapExerciser.testResolveAsCannotReproduce(ISSUE_KEY);
        gotoIssue(ISSUE_KEY);
        assertTextPresent("Cannot Reproduce");
    }


    /**
     * JRA-16112 and JRA-16915
     *
     * @throws RemoteException if stuff goes wrong
     */
    public void testResolveIssueWithNoResolution() throws RemoteException
    {
        gotoIssue(ISSUE_KEY);
        assertTextPresent("Unresolved");
        workflowSoapExerciser.testResolveWithNoResolution(ISSUE_KEY);
        gotoIssue(ISSUE_KEY);
        //issue should have been resolved with the default resolution of 'Fixed'.
        assertTextSequence(new String[] { "Status", "Resolved","Resolution:", "Fixed" });
    }

    /**
     * Very simple test that ensures that if an issue is transitioned to the same resolution
     * without changing a single field value, all values are preserved.
     */
    public void testTransitionIssueToSameResolution() throws RemoteException
    {
        restoreData("jira-soap_client_func_test_transition_workflow.xml");
        soapConnect();
        assertFieldValues("HSP-2", ISSUE_DETAILS_FIELD_VALUE_MAP, FIELD_VALUE_MAP);

        try
        {
            workflowSoapExerciser.testProgressWorkflow("HSP-2", "711");
        }
        catch (RemoteException e)
        {
            fail("Remote exception when progressing workflow: " + e);
        }
        assertFieldValues("HSP-2", ISSUE_DETAILS_FIELD_VALUE_MAP, FIELD_VALUE_MAP);
    }

    /**
     * Iterates through all the fields, and changes each field one at a time, then checks that
     * the change has been made and all other fields still have their previous values.
     *
     * @throws RemoteException
     */
    public void testTransitionAllFieldsOneAtATime() throws RemoteException
    {
        restoreData("jira-soap_client_func_test_transition_workflow.xml");
        soapConnect();
        assertFieldValues("HSP-2", ISSUE_DETAILS_FIELD_VALUE_MAP, FIELD_VALUE_MAP);

        final Map<String, Object> issueDetails = new LinkedHashMap<String, Object>(ISSUE_DETAILS_FIELD_VALUE_MAP);
        final Map<String, Object> fieldDetails = new LinkedHashMap<String, Object>(FIELD_VALUE_MAP);
        for (Map.Entry<String, FieldDetail> fieldUpdateEntry : FIELD_UPDATE_MAP.entrySet())
        {
            String fieldId = fieldUpdateEntry.getKey();
            FieldDetail fieldDetail = fieldUpdateEntry.getValue();
            log("Updating field '" + fieldId + "' with '" + fieldDetail);

            try
            {
                RemoteFieldValue[] actionParams;
                if (fieldDetail.optionalId == null)
                {
                    actionParams = new RemoteFieldValue[] {
                            new RemoteFieldValue(fieldId, fieldDetail.fieldUpdateValue)
                    };
                }
                else
                {
                    //cascading selects are a bitch!
                    actionParams = new RemoteFieldValue[] {
                            new RemoteFieldValue(fieldId, new String[] { fieldDetail.fieldUpdateValue[0] }),
                            new RemoteFieldValue(fieldDetail.optionalId, new String[] { fieldDetail.fieldUpdateValue[1] })
                    };

                }

                workflowSoapExerciser.testProgressWorkflow("HSP-2", "711", actionParams);
            }
            catch (RemoteException e)
            {
                fail("Remote exception when progressing workflow: " + e);
            }

            gotoIssue("HSP-2");
            if (fieldDetail.isIssueDetail)
            {
                issueDetails.put(fieldDetail.getFieldName(), Arrays.asList(fieldDetail.getFieldUpdateName()));
            }
            else
            {
                fieldDetails.put(fieldDetail.getFieldName(), Arrays.asList(fieldDetail.getFieldUpdateName()));
            }
            assertFieldValues("HSP-2", issueDetails, fieldDetails);
        }
    }

    /**
     * Tests that each field can be nulled out one at a time.  Also checks that the other field's values
     * are preserved.
     *
     * @throws RemoteException
     */
    public void testEmptyAllFieldsOneAtATimeDuringTransition() throws RemoteException
    {
        restoreData("jira-soap_client_func_test_transition_workflow.xml");
        soapConnect();
        assertFieldValues("HSP-2", ISSUE_DETAILS_FIELD_VALUE_MAP, FIELD_VALUE_MAP);

        final Map<String, Object> issueDetails = new LinkedHashMap<String, Object>(ISSUE_DETAILS_FIELD_VALUE_MAP);
        final Map<String, Object> fieldDetails = new LinkedHashMap<String, Object>(FIELD_VALUE_MAP);
        for (Map.Entry<String, FieldDetail> fieldUpdateEntry : FIELD_UPDATE_MAP.entrySet())
        {
            String fieldId = fieldUpdateEntry.getKey();
            FieldDetail fieldDetail = fieldUpdateEntry.getValue();
            log("Setting field '" + fieldId + "' to null.");

            try
            {
                RemoteFieldValue[] actionParams = new RemoteFieldValue[] {
                        new RemoteFieldValue(fieldId, new String [] {})
                };

                workflowSoapExerciser.testProgressWorkflow("HSP-2", "711", actionParams);
            }
            catch (RemoteException e)
            {
                if(e instanceof com.atlassian.jira.rpc.soap.client.RemoteException)
                {
                    com.atlassian.jira.rpc.soap.client.RemoteException re = (com.atlassian.jira.rpc.soap.client.RemoteException) e;
                    if(!re.getFaultString().contains("resolution=Resolution is required"))
                    {
                        fail("Remote exception when progressing workflow: " + re);
                    }
                }
                else
                {
                    throw e;
                }

            }

            gotoIssue("HSP-2");
            if (fieldDetail.isIssueDetail)
            {
                //guess what: Priority is not a required field by default...
                if(fieldId.startsWith("priority"))
                {
                    issueDetails.remove(fieldDetail.getFieldName());
                }
                else
                {
                    issueDetails.put(fieldDetail.getFieldName(), "");
                }
            }
            else
            {
                //custom fields should disappear when they don't have a value.
                //environment will also disappear.
                if(fieldId.startsWith("customfield_")  || fieldId.startsWith("environment"))
                {
                    fieldDetails.remove(fieldDetail.getFieldName());
                }
                else
                {
                    fieldDetails.put(fieldDetail.getFieldName(), "");
                }
            }
            assertFieldValues("HSP-2", issueDetails, fieldDetails);
        }
    }

    private void assertFieldValues(String issueKey, Map<String, Object> issueDetails, Map<String, Object> fieldDetails)
    {
        gotoIssue(issueKey);
        final List<String> issueDetailAssertions = getTextAssertions(issueDetails);
        assertTextSequence(issueDetailAssertions.toArray(new String[issueDetailAssertions.size()]));

        final List<String> textAssertions = getTextAssertions(fieldDetails);
        //assert all fields have the correct value!
        assertTextSequence(textAssertions.toArray(new String[textAssertions.size()]));
    }

    private List<String> getTextAssertions(final Map<String, Object> fieldValueMap)
    {
        final List<String> textAssertions = new ArrayList<String>();
        for (Map.Entry<String, Object> fieldEntry : fieldValueMap.entrySet())
        {
            textAssertions.add(fieldEntry.getKey());
            if (fieldEntry.getValue() != null)
            {
                if (fieldEntry.getValue() instanceof Collection)
                {
                    textAssertions.addAll((Collection<? extends String>) fieldEntry.getValue());
                }
                else
                {
                    textAssertions.add((String) fieldEntry.getValue());
                }
            }
        }
        return textAssertions;
    }
}
