package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.JiraSoapService;
import com.atlassian.jira.rpc.soap.client.RemoteAvatar;
import com.atlassian.jira.rpc.soap.client.RemoteIssueType;
import com.atlassian.jira.rpc.soap.client.RemotePermission;
import com.atlassian.jira.rpc.soap.client.RemotePermissionException;
import com.atlassian.jira.rpc.soap.client.RemotePermissionScheme;
import com.atlassian.jira.rpc.soap.client.RemoteProject;
import com.atlassian.jira.rpc.soap.client.RemoteScheme;
import com.atlassian.jira.rpc.soap.client.RemoteVersion;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.ProjectClient;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.ProjectRole;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.ProjectRoleClient;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.Version;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.VersionClient;
import com.atlassian.jira_soapclient.exercise.ExerciserClientConstants;
import org.apache.commons.codec.binary.Base64;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;

import static com.atlassian.jira.webtests.Groups.ADMINISTRATORS;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;
import static java.awt.BasicStroke.CAP_ROUND;
import static java.awt.BasicStroke.JOIN_ROUND;
import static java.awt.Color.GREEN;
import static java.awt.Color.ORANGE;
import static java.awt.RenderingHints.KEY_ANTIALIASING;
import static java.awt.RenderingHints.VALUE_ANTIALIAS_ON;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;

public class TestProjectAdminSoapExerciser extends SOAPTestCase
{
    // Constants for Versions
    public static String HSP_VERSION_NAME_1 = "New Version 1";
    public static String HSP_VERSION_NAME_4 = "New Version 4";
    public static String HSP_VERSION_NAME_5 = "New Version 5";

    public void testCreateProject(String projectKey, String projectName, String projectDesc, String projectLead)
            throws RemoteException
    {
        restoreData("jira_soap_client_func_test_create_project.xml");
        soapConnect();

        //Create a remoteProject to create
        RemoteProject project = new RemoteProject();
        project.setKey(projectKey);
        project.setLead(projectLead);
        project.setName(projectName);
        project.setDescription(projectDesc);
        RemotePermissionScheme defaultPermScheme = new RemotePermissionScheme();
        defaultPermScheme.setId(0L);
        project.setPermissionScheme(defaultPermScheme);

        //create the project
        RemoteProject returnedProject = projectAdminSoapExerciser.testCreateProjectFromObject(project);

        //assert that the created project and the returned project are not equal (the returned one has an id) JRA-12302
        assertNull(project.getId());
        assertNotNull(returnedProject.getId());
        assertEquals(project.getKey(), returnedProject.getKey());
        assertEquals(project.getLead(), returnedProject.getLead());
        assertEquals(project.getName(), returnedProject.getName());
        assertEquals(project.getDescription(), returnedProject.getDescription());


        //check that the project role is set to the default roles
        ProjectRoleClient prc = new ProjectRoleClient(environmentData);
        final Map roles = prc.get(project.getKey());
        assertEquals(3, roles.size());

        ProjectRole projectRole = prc.get(project.getKey(), "Administrators");
        assertEquals(2, projectRole.actors.size());
        assertEquals(ADMINISTRATORS, projectRole.actors.get(0).name);
        assertEquals("admin", projectRole.actors.get(1).name);

        projectRole = prc.get(project.getKey(), "Developers");
        assertEquals(2, projectRole.actors.size());
        assertEquals(DEVELOPERS, projectRole.actors.get(0).name);
        assertEquals("dev", projectRole.actors.get(1).name);

        projectRole = prc.get(project.getKey(), "Users");
        assertEquals(2, projectRole.actors.size());
        assertEquals(USERS, projectRole.actors.get(0).name);
        assertEquals("user", projectRole.actors.get(1).name);

        // assert that the project exists
        gotoPage("/secure/BrowseProject.jspa");
        assertTextPresent(projectName);

        // now test that we can delete the project
        projectAdminSoapExerciser.testDeleteProject(projectKey);
        gotoPage("/secure/project/ViewProjects.jspa");
        assertTextNotPresent(projectDesc);
    }

    public void testGetProjectWithSchemes() throws RemoteException
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        final RemoteProject[] projects = service.getProjectsNoSchemes(getToken());

        final RemoteProject projectWithoutSchemes = projects[0];
        assertNull(projectWithoutSchemes.getIssueSecurityScheme());
        assertNull(projectWithoutSchemes.getNotificationScheme());
        assertNull(projectWithoutSchemes.getPermissionScheme());

        final RemoteProject projectWithSchemes = service.getProjectWithSchemesById(getToken(), Long.valueOf(projectWithoutSchemes.getId()));
        assertNull(projectWithSchemes.getIssueSecurityScheme());
        assertNull(projectWithSchemes.getNotificationScheme());
        assertNotNull(projectWithSchemes.getPermissionScheme());
    }


    public void testGetProjectById() throws RemoteException
    {
        final RemoteProject project = projectAdminSoapExerciser.testGetProjectById(PROJECT_ID);
        assertNotNull("Failed to retrieve RemoteProject", project);
        assertEquals("homosapien", project.getName());
        assertEquals("admin", project.getLead());
        assertEquals("HSP", project.getKey());
        assertEquals("project for homosapiens", project.getDescription());
    }

    public void testGetProjectByKey() throws RemoteException
    {
        final RemoteProject project = projectAdminSoapExerciser.testGetProjectByKey(PROJECT_KEY);
        assertNotNull("Failed to retrieve RemoteProject", project);
        assertEquals("homosapien", project.getName());
        assertEquals("admin", project.getLead());
        assertEquals("10000", project.getId());
        assertEquals("project for homosapiens", project.getDescription());
    }


    public void testCreateVersion() throws RemoteException
    {
        projectAdminSoapExerciser.testCreateVersion();

        ProjectClient pc= new ProjectClient(environmentData);
        final List<Version> versions = pc.getVersions(PROJECT_HOMOSAP_KEY);
        for (Version version : versions)
        {
            if (version.name.startsWith("3 SOAP created version "))
            {
                return;
            }
        }
        fail("Version was not created");
    }

    public void testGetAllPermissions() throws RemoteException
    {
        RemotePermission[] allPermissions = jiraAdminSoapExerciser.testGetAllPermissions();
        assertEquals(31, allPermissions.length);
    }

    public void testDeleteProject() throws RemoteException
    {
        projectAdminSoapExerciser.testDeleteProject(PROJECT_HOMOSAP_KEY);
        gotoAdmin();
        assertTextNotPresent(PROJECT_HOMOSAP_KEY);
    }

    public void testDeleteProjectWithPortlet() throws Exception
    {
        restoreData("DeleteProjectWithPortlet.xml");
        soapConnect();
        projectAdminSoapExerciser.testDeleteProject(PROJECT_HOMOSAP_KEY);
        gotoAdmin();
        assertTextNotPresent(PROJECT_HOMOSAP_KEY);
        navigation.gotoDashboard();
        assertTextNotPresent(PROJECT_HOMOSAP_KEY);
    }

    public void testGetIssueTypesAndSubTasksForProject() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_issue_types_for_project.xml");

        //get all issue types of project MKY with a non-default issue type scheme
        RemoteIssueType[] types = projectAdminSoapExerciser.getIssueTypesForProject(ExerciserClientConstants.LOGIN_NAME, ExerciserClientConstants.LOGIN_PASSWORD, "10001");
        assertExpectedIssueTypes(types, new String[] { "Bug", "Unique", "Task" }, false);

        //get all subtask issue types of project MKY with a non-default issue type scheme
        types = projectAdminSoapExerciser.getSubTaskIssueTypesForProject(ExerciserClientConstants.LOGIN_NAME, ExerciserClientConstants.LOGIN_PASSWORD, "10001");
        assertExpectedIssueTypes(types, new String[] { "SubTask2" }, true);

        //get all issue types of project HSP with a default issue type scheme
        types = projectAdminSoapExerciser.getIssueTypesForProject(ExerciserClientConstants.LOGIN_NAME, ExerciserClientConstants.LOGIN_PASSWORD, "10000");
        assertExpectedIssueTypes(types, new String[] { "Bug", "New Feature", "Task", "Improvement", "Unique" }, false);

        //get all subtask issue types of project HSP with a default issue type scheme
        types = projectAdminSoapExerciser.getSubTaskIssueTypesForProject(ExerciserClientConstants.LOGIN_NAME, ExerciserClientConstants.LOGIN_PASSWORD, "10000");
        assertExpectedIssueTypes(types, new String[] { "Sub-task", "SubTask2" }, true);

        //get all issue types of default issue type scheme
        types = jiraAdminSoapExerciser.getIssueTypes(ExerciserClientConstants.LOGIN_NAME, ExerciserClientConstants.LOGIN_PASSWORD);
        assertExpectedIssueTypes(types, new String[] { "Bug", "New Feature", "Task", "Improvement", "Unique" }, false);

        //get all subtask issue types of default issue type scheme
        types = jiraAdminSoapExerciser.getSubTaskIssueTypes(ExerciserClientConstants.LOGIN_NAME, ExerciserClientConstants.LOGIN_PASSWORD);
        assertExpectedIssueTypes(types, new String[] { "Sub-task", "SubTask2" }, true);

        //try to get the issue types of a non-existant project (returns no issue types)
        types = projectAdminSoapExerciser.getIssueTypesForProject(ExerciserClientConstants.LOGIN_NAME, ExerciserClientConstants.LOGIN_PASSWORD, "1");
        assertEquals(0, types.length);

        //try to get the issue types of a project with null id (returns no issue types)
        types = projectAdminSoapExerciser.getIssueTypesForProject(ExerciserClientConstants.LOGIN_NAME, ExerciserClientConstants.LOGIN_PASSWORD, null);
        assertEquals(0, types.length);

        try
        {
            projectAdminSoapExerciser.getIssueTypesForProject(ExerciserClientConstants.LOGIN_NAME, ExerciserClientConstants.LOGIN_PASSWORD, "numberFormatException");
            fail();
        }
        catch (RemoteException e)
        {
            // This should happen as the id is not in a number format
            assertTrue(e.getMessage().contains("NumberFormatException"));
        }

        try
        {
            // Try to get the issue types as fred who does not have permission to do so
            projectAdminSoapExerciser.getIssueTypesForProject("fred", "fred", "10000");
            fail();
        }
        catch (RemotePermissionException rpe)
        {
            // This should happen as the user does not have permission
        }
    }

    private void assertExpectedIssueTypes(RemoteIssueType[] types, String[] expectedTypeNamesArray, boolean isSubTasks)
            throws RemoteException
    {
        assertEquals(expectedTypeNamesArray.length, types.length);
        List<String> expectedTypeNames = new ArrayList<String>(Arrays.asList(expectedTypeNamesArray));
        for (RemoteIssueType type : types)
        {
            assertTrue(expectedTypeNames.remove(type.getName()));
            assertEquals(isSubTasks, type.isSubTask());
        }
        assertTrue(expectedTypeNames.isEmpty());
    }


    public void testArchiveVersion() throws RemoteException
    {
        projectAdminSoapExerciser.testArchiveVersion(ExerciserClientConstants.PROJECT_KEY, HSP_VERSION_NAME_1);
        VersionClient vc = new VersionClient(environmentData);
        final Version version = vc.get("10000");
        assertTrue(version.archived);

    }

    public void testReleaseVersion() throws RemoteException
    {
        final RemoteVersion version = new RemoteVersion();
        version.setName(HSP_VERSION_NAME_1);
        version.setReleased(true);

        Calendar releaseDate = Calendar.getInstance();
        releaseDate.set(Calendar.MONTH, Calendar.DECEMBER);
        releaseDate.set(Calendar.YEAR, 2006);
        releaseDate.set(Calendar.DATE, 25);
        version.setReleaseDate(releaseDate);

        projectAdminSoapExerciser.testReleaseVersion(version, ExerciserClientConstants.PROJECT_KEY);

        RemoteVersion[] remoteVersions = projectAdminSoapExerciser.getJiraSoapService().getVersions(getToken(), ExerciserClientConstants.PROJECT_KEY);
        boolean foundVersion = false;
        for (RemoteVersion remoteVersion : remoteVersions)
        {
           if (HSP_VERSION_NAME_1.equals(remoteVersion.getName()))
           {
              foundVersion = true;
              assertTrue(remoteVersion.isReleased());
              assertFalse(remoteVersion.isArchived());
              assertEquals(releaseDate.getTime(), remoteVersion.getReleaseDate().getTime());
           }
        }
        assertTrue(foundVersion);
    }

    public void testUnReleaseVersion() throws RemoteException
    {
        final RemoteVersion releaseVersion = new RemoteVersion();
        releaseVersion.setName(HSP_VERSION_NAME_1);
        releaseVersion.setReleased(true);

        projectAdminSoapExerciser.getJiraSoapService().releaseVersion(getToken(), ExerciserClientConstants.PROJECT_KEY, releaseVersion);

        final RemoteVersion version = new RemoteVersion();
        version.setName(HSP_VERSION_NAME_1);
        version.setReleased(false);

        Calendar releaseDate = Calendar.getInstance();
        releaseDate.set(Calendar.MONTH, Calendar.DECEMBER);
        releaseDate.set(Calendar.YEAR, 2006);
        releaseDate.set(Calendar.DATE, 25);
        version.setReleaseDate(releaseDate);

        projectAdminSoapExerciser.testReleaseVersion(version, ExerciserClientConstants.PROJECT_KEY);
        RemoteVersion[] versions = projectAdminSoapExerciser.getJiraSoapService().getVersions(getToken(), ExerciserClientConstants.PROJECT_KEY);
        boolean foundVersion = false;
        for (RemoteVersion remoteVersion : versions)
        {
            if (HSP_VERSION_NAME_1.equals(remoteVersion.getName()))
            {
                foundVersion = true;
                assertFalse(remoteVersion.isReleased());
                assertEquals(releaseDate.getTime(), remoteVersion.getReleaseDate().getTime());
            }
        }
        assertTrue(foundVersion);
    }

    public void testUnReleaseVersionWithNoReleaseDate() throws RemoteException
    {
        //JRA-11062: test patch that was included by user to satisfy his use case

        //1. create a version with a release date
        RemoteVersion remoteVersion = projectAdminSoapExerciser.testCreateVersion();

        //2. release it
        remoteVersion.setReleased(true);
        projectAdminSoapExerciser.testReleaseVersion(remoteVersion, ExerciserClientConstants.PROJECT_KEY);


        VersionClient vc = new VersionClient(environmentData);
        Version version = vc.get(remoteVersion.getId());
        assertTrue(version.released);

        //3. unrelease it & clear release date
        remoteVersion.setReleased(false);

        Calendar releaseDate = remoteVersion.getReleaseDate();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMM/yy");
        String shownReleaseDate = simpleDateFormat.format(releaseDate.getTime());

        remoteVersion.setReleaseDate(null);
        projectAdminSoapExerciser.testReleaseVersion(remoteVersion, ExerciserClientConstants.PROJECT_KEY);

        version = vc.get(remoteVersion.getId());
        assertFalse(version.released);
        assertTrue(version.releaseDate == null);
    }

    public void testGetVersions() throws RemoteException
    {
        //first try to get them as an admin.  SHould have no issues!
        RemoteVersion[] remoteVersions = projectAdminSoapExerciser.testGetVersions(ExerciserClientConstants.PROJECT_KEY);
        assertEquals(3, remoteVersions.length);
        assertEquals("New Version 1", remoteVersions[0].getName());
        assertEquals("New Version 4", remoteVersions[1].getName());
        assertEquals("New Version 5", remoteVersions[2].getName());

        //check a user with only BROWSE permission can see the versions
        soapConnect("fred", "fred");
        remoteVersions = projectAdminSoapExerciser.testGetVersions(ExerciserClientConstants.PROJECT_KEY);
        assertEquals(3, remoteVersions.length);
        assertEquals("New Version 1", remoteVersions[0].getName());
        assertEquals("New Version 4", remoteVersions[1].getName());
        assertEquals("New Version 5", remoteVersions[2].getName());

        //now setup 'fred' such that he doesn't have BROWSE permission for HSP
        tester.gotoPage("/secure/admin/jira/GlobalPermissions!default.jspa");
        tester.selectOption("permType", "JIRA Users");
        tester.selectOption("groupName", "jira-developers");
        tester.submit("Add");

        tester.gotoPage("/secure/admin/user/EditUserGroups!default.jspa?name=fred");
        tester.selectOption("groupsToJoin", "jira-developers");
        tester.submit("join");
        tester.selectOption("groupsToLeave", "jira-users");
        tester.submit("leave");

        soapConnect("fred", "fred");
        try
        {
            projectAdminSoapExerciser.testGetVersions(ExerciserClientConstants.PROJECT_KEY);
            fail("Should have thrown exception about requiring browse permission for the project");
        }
        catch (RemoteException e)
        {
            //yay!!
        }
    }


    /*
     * This test should just release the version and not pay attention to the archive call.
     */
    public void testTryToReleaseAndArchiveAVersion() throws RemoteException
    {
        final RemoteVersion version = new RemoteVersion();
        version.setName(HSP_VERSION_NAME_1);

        version.setReleased(true);
        version.setArchived(true);

        Calendar releaseDate = Calendar.getInstance();
        releaseDate.set(Calendar.MONTH, Calendar.DECEMBER);
        releaseDate.set(Calendar.YEAR, 2006);
        releaseDate.set(Calendar.DATE, 25);
        version.setReleaseDate(releaseDate);

        projectAdminSoapExerciser.testReleaseVersion(version, ExerciserClientConstants.PROJECT_KEY);
        RemoteVersion[] versions = projectAdminSoapExerciser.getJiraSoapService().getVersions(getToken(), ExerciserClientConstants.PROJECT_KEY);
        boolean foundVersion = false;
        for (RemoteVersion remoteVersion : versions)
        {
           if (HSP_VERSION_NAME_1.equals(remoteVersion.getName()))
           {
              foundVersion = true;
              assertTrue(remoteVersion.isReleased());
              assertFalse(remoteVersion.isArchived());
              assertEquals(releaseDate.getTime(), remoteVersion.getReleaseDate().getTime());
           }
        }
        assertTrue(foundVersion);
    }

    public void testAddPermissionScheme() throws RemoteException
    {
        gotoAdmin();
        gotoPage("/plugins/servlet/project-config/HSP/permissions");
        assertions.assertNodeByIdHasText("project-config-permissions-scheme-name", "Default Permission Scheme");

        final JiraSoapService service = soapSession.getJiraSoapService();
        final RemoteProject project = getRemoteProjectWithSchemes(service);
        final RemotePermissionScheme remotePermissionScheme = service.createPermissionScheme(getToken(), "TestPermissionScheme", "Sample");
        project.setPermissionScheme(remotePermissionScheme);
        service.updateProject(getToken(), project);

        gotoPage("/plugins/servlet/project-config/HSP/permissions");
        assertions.assertNodeByIdHasText("project-config-permissions-scheme-name", "TestPermissionScheme");
    }

    public void testAddNotificationScheme() throws RemoteException
    {
        gotoAdmin();
        gotoPage("/plugins/servlet/project-config/HSP/notifications");
        assertions.assertNodeByIdHasText("project-config-notification-scheme-name", "None");

        final JiraSoapService service = soapSession.getJiraSoapService();
        RemoteScheme[] schemes = service.getNotificationSchemes(getToken());
        assertEquals(1, schemes.length);

        final RemoteProject project = getRemoteProjectWithSchemes(service);
        project.setNotificationScheme(schemes[0]);
        service.updateProject(getToken(), project);

        gotoPage("/plugins/servlet/project-config/HSP/notifications");
        assertions.assertNodeByIdHasText("project-config-notification-scheme-name", "Default Notification Scheme");

        //lets create a new notification scheme.
        gotoAdmin();
        clickLink("notification_schemes");
        clickLinkWithText("Add Notification Scheme");
        setFormElement("name", "LoudNotificationScheme");
        submit("Add");

        schemes = service.getNotificationSchemes(getToken());
        assertEquals(2, schemes.length);

        //ensure we're using the correct scheme.
        if (schemes[1].getName().contains("Loud"))
        {
            project.setNotificationScheme(schemes[1]);
        }
        else
        {
            project.setNotificationScheme(schemes[0]);
        }
        service.updateProject(getToken(), project);

        gotoPage("/plugins/servlet/project-config/HSP/notifications");
        assertions.assertNodeByIdHasText("project-config-notification-scheme-name", "LoudNotificationScheme");
    }

    public void testAddIssueSecurityScheme() throws RemoteException
    {
        gotoAdmin();
        clickLink("security_schemes");
        clickLink("add_securityscheme");
        setFormElement("name", "NewIssueSecScheme");
        submit("Add");
        clickLink("security_schemes");
        clickLink("add_securityscheme");
        setFormElement("name", "AnotherIssueSecScheme");
        submit("Add");

        gotoPage("/plugins/servlet/project-config/HSP/issuesecurity");
        assertions.assertNodeByIdHasText("project-config-issuesecurity-scheme-name", "Anyone");

        final JiraSoapService service = soapSession.getJiraSoapService();
        RemoteScheme[] schemes = service.getSecuritySchemes(getToken());
        assertEquals(2, schemes.length);

        RemoteScheme newScheme;
        RemoteScheme anotherScheme;
        if (schemes[0].getName().contains(("New")))
        {
            newScheme = schemes[0];
            anotherScheme = schemes[1];
        }
        else
        {
            newScheme = schemes[1];
            anotherScheme = schemes[0];
        }

        RemoteProject project = getRemoteProjectWithSchemes(service);
        project.setIssueSecurityScheme(newScheme);
        service.updateProject(getToken(), project);

        gotoPage("/plugins/servlet/project-config/HSP/issuesecurity");
        assertions.assertNodeByIdHasText("project-config-issuesecurity-scheme-name", "NewIssueSecScheme");

        schemes = service.getSecuritySchemes(getToken());
        assertEquals(2, schemes.length);

        project = getRemoteProjectWithSchemes(service);
        project.setIssueSecurityScheme(anotherScheme);
        service.updateProject(getToken(), project);

        gotoPage("/plugins/servlet/project-config/HSP/issuesecurity");
        assertions.assertNodeByIdHasText("project-config-issuesecurity-scheme-name", "AnotherIssueSecScheme");
    }

    public void testGetAvatars() throws RemoteException
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        final RemoteAvatar[] remoteAvatars = service.getProjectAvatars(getToken(), "HSP", true);
        for (RemoteAvatar remoteAvatar : remoteAvatars)
        {
            assertEquals("image/png", remoteAvatar.getContentType());
            assertTrue(remoteAvatar.getBase64Data().length() > 2000);
        }
        assertEquals("Did not expect any custom avatars to be there", 0, service.getProjectAvatars(getToken(), "HSP", false).length);
    }

    public void testSetAvatar() throws Exception
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        assertEquals("Did not expect any custom avatars to be there", 0, service.getProjectAvatars(getToken(), "HSP", false).length);

        final byte[] imageBytes = makeDemoPng();
        final String encodedImage = base64Encode(imageBytes);
        service.setNewProjectAvatar(getToken(), "HSP", "image/png", encodedImage);
        final RemoteAvatar retrieved = service.getProjectAvatars(getToken(), "HSP", false)[0];
        assertEquals("image/png", retrieved.getContentType());
        assertEquals("10000", retrieved.getOwner());
        byte[] retreivedData = base64Decode(retrieved.getBase64Data());
        //assert the lengths of the images are similar. we can't be exact because the image is resampled.
        assertTrue(Math.abs(retreivedData.length - imageBytes.length) < 50);
    }

    public void testGetAvatar() throws Exception
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        RemoteAvatar remoteAvatar = service.getProjectAvatar(getToken(), "HSP");
        // should be default avatar
        assertEquals(10011, remoteAvatar.getId());
        assertEquals(null, remoteAvatar.getOwner());
        assertEquals("image/png", remoteAvatar.getContentType());
        assertEquals(true, remoteAvatar.isSystem());
        // low value fuzzy assertion about the base64 encoded size of the default system avatar - should catch incorrect pizel size and empty data results
        assertTrue(Math.abs(4000 - remoteAvatar.getBase64Data().length()) < 1000);

        // now change
        service.setProjectAvatar(getToken(), "HSP", 10010);

        // now assert changed
        remoteAvatar = service.getProjectAvatar(getToken(), "HSP");
        assertEquals(10010, remoteAvatar.getId()); // kangaroo sign
        assertEquals(null, remoteAvatar.getOwner());
        assertEquals("image/png", remoteAvatar.getContentType());
        assertEquals(true, remoteAvatar.isSystem());
        // low value fuzzy assertion about the base64 encoded size of this system avatar - should catch incorrect pixel size and empty data results
        assertTrue(Math.abs(8000 - remoteAvatar.getBase64Data().length()) < 1000);
    }

    public void testDeleteSystemAvatar() throws Exception
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        RemoteAvatar defaultAvatar = service.getProjectAvatar(getToken(), "HSP");
        // try to delete default, it will be denied

        try
        {
            service.deleteProjectAvatar(getToken(), defaultAvatar.getId());
            fail("expected RemoteException");
        }
        catch (RemoteException yay)
        {

        }

    }

    public void testDeleteCustomAvatar() throws Exception
    {
        final JiraSoapService service = soapSession.getJiraSoapService();
        RemoteAvatar originalAvatar = service.getProjectAvatar(getToken(), "HSP");

        final byte[] imageBytes = makeDemoPng();
        final String encodedImage = base64Encode(imageBytes);
        service.setNewProjectAvatar(getToken(), "HSP", "image/png", encodedImage);
        final RemoteAvatar updated = service.getProjectAvatar(getToken(), "HSP");
        assertFalse("Expected setProjectAvatar to have resulted in a new id", originalAvatar.getId() == updated.getId());
        service.deleteProjectAvatar(getToken(), updated.getId());
        final RemoteAvatar reverted = service.getProjectAvatar(getToken(), "HSP");
        assertEquals(originalAvatar.getId(), reverted.getId());
    }


    private byte[] makeDemoPng() throws IOException
    {
        BufferedImage image = new BufferedImage(48, 48, TYPE_INT_ARGB);
        final Graphics2D graphics = image.createGraphics();
        graphics.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);
        graphics.setColor(GREEN.darker());
        graphics.fillRect(0, 0, 48, 48);
        graphics.setStroke(new BasicStroke(4f, CAP_ROUND, JOIN_ROUND, 10F, new float[] { 3f, 7f }, 0f));
        graphics.setColor(ORANGE.brighter());
        graphics.drawOval(8, 8, 32, 32);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "PNG", baos);
        return baos.toByteArray();
    }

    private String base64Encode(final byte[] encodeMe) throws UnsupportedEncodingException
    {
        return new String(Base64.encodeBase64(encodeMe), "UTF-8");
    }

    private byte[] base64Decode(final String base64Data) throws UnsupportedEncodingException
    {
        return Base64.decodeBase64(base64Data.getBytes("UTF-8"));
    }

    private RemoteProject getRemoteProjectWithSchemes(final JiraSoapService service) throws RemoteException
    {
        RemoteProject[] projects = service.getProjectsNoSchemes(getToken());
        return service.getProjectWithSchemesById(getToken(), Long.valueOf(projects[0].getId()));
    }
}