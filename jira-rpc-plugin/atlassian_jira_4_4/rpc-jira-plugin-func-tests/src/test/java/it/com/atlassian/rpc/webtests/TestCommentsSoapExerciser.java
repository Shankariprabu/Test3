package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteComment;

import java.rmi.RemoteException;

/**
 *
 */
public class TestCommentsSoapExerciser extends SOAPTestCase
{
    protected static final String ROLE_ADMINISTRATORS = "Administrators";
    protected static final String GROUP_JIRA_USERS = "jira-users";
    // Constant for add comment
    protected static String NEW_COMMENT_BODY = "This is a new comment";
    protected static String EDIT_COMMENT_BODY = "This is the body of a comment that has been edited";

    public void testAddCommentToIssue() throws RemoteException
    {
        try
        {
            //test adding a blank comment (JRA-11484)
            commentsSoapExerciser.testAddBlankComment(ISSUE_KEY);
            fail("Exception expected.");
        }
        catch (RemoteException yay)
        {
            assertTrue(yay.toString().indexOf("Comment body can not be empty") > -1);
        }

        // Test adding a normal comment
        commentsSoapExerciser.testAddComment(ISSUE_KEY, NEW_COMMENT_BODY);
        gotoIssue(ISSUE_KEY);
        assertTextPresent(NEW_COMMENT_BODY);
    }

    public void testAddCommentVisibilityValidations()
    {
        final String NO_SUCH_ROLE = "nosuchrole";
        final String NO_SUCH_GROUP = "nosuchgroup";

        try
        {
            //test adding a comment set to both group and roles
            commentsSoapExerciser.testAddCommentWithVisibility(ISSUE_KEY, GROUP_JIRA_USERS, ROLE_ADMINISTRATORS, NEW_COMMENT_BODY);
            fail("Exception expected.");
        }
        catch (RemoteException yay)
        {
            assertTrue(yay.toString().indexOf("Selecting comment visibility can be for group or role, not both!") > -1);
        }

        try
        {
            //test adding a comment set to a non-existing group
            commentsSoapExerciser.testAddCommentWithVisibility(ISSUE_KEY, NO_SUCH_GROUP, null, NEW_COMMENT_BODY);
            fail("Exception expected.");
        }
        catch (RemoteException yay)
        {
            assertTrue(yay.toString().indexOf("Group: " + NO_SUCH_GROUP + " does not exist.") > -1);
        }

        try
        {
            //test adding a comment set to a non-existing role
            commentsSoapExerciser.testAddCommentWithVisibility(ISSUE_KEY, null, NO_SUCH_ROLE, NEW_COMMENT_BODY);
            fail("Exception expected.");
        }
        catch (RemoteException yay)
        {
            assertTrue(yay.toString().indexOf("Project role: " + NO_SUCH_ROLE + " does not exist") > -1);
        }

        try
        {
            //test adding a comment set to a role user is not a member of
            commentsSoapExerciser.testAddCommentWithVisibility(ISSUE_KEY, null, ROLE_ADMINISTRATORS, NEW_COMMENT_BODY);
            fail("Exception expected.");
        }
        catch (RemoteException yay)
        {
            assertTrue(yay.toString().indexOf("You are currently not a member of the project role: " + ROLE_ADMINISTRATORS) > -1);
        }

        final String NEW_GROUP_WITH_NO_USERS = "new group with no users";
        try
        {
            createGroup(NEW_GROUP_WITH_NO_USERS);
            //test adding a comment set to a group user is not a member of
            commentsSoapExerciser.testAddCommentWithVisibility(ISSUE_KEY, NEW_GROUP_WITH_NO_USERS, null, NEW_COMMENT_BODY);
            fail("Exception expected.");
        }
        catch (RemoteException yay)
        {
            assertTrue(yay.toString().indexOf("You are currently not a member of the group: " + NEW_GROUP_WITH_NO_USERS) > -1);
        }
    }

    public void testGetComments() throws RemoteException
    {
        final String issueKey = addIssue(PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, "Bug", "test get comments");

        //the newly created issue should have no issues
        RemoteComment[] remoteComments = commentsSoapExerciser.testGetComments(issueKey);
        assertEquals(0, remoteComments.length);

        //add a comment and check that it is returned
        navigation.issue().addComment(issueKey, "comment 1", null);
        remoteComments = commentsSoapExerciser.testGetComments(issueKey);
        assertEquals(1, remoteComments.length);
        assertCommentContent(remoteComments[0], "comment 1", null, null);

        //add more comments and assert their contents
        navigation.issue().addComment(issueKey, "comment 2", GROUP_JIRA_USERS);
        addUserToProjectRole(ADMIN_USERNAME, PROJECT_HOMOSAP, ROLE_ADMINISTRATORS);
        navigation.issue().addComment(issueKey, "comment 3", ROLE_ADMINISTRATORS);
        remoteComments = commentsSoapExerciser.testGetComments(issueKey);
        assertEquals(3, remoteComments.length);
        assertCommentContent(remoteComments[0], "comment 1", null, null);
        assertCommentContent(remoteComments[1], "comment 2", GROUP_JIRA_USERS, null);
        assertCommentContent(remoteComments[2], "comment 3", null, ROLE_ADMINISTRATORS);
    }

    public void testEditComment() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_edit_comment.xml");
        soapConnect();

        RemoteComment comment = createAndRetrieveCommentOnIssue("HSP-2");

        // Now lets edit the comment
        commentsSoapExerciser.testEditComment(comment, EDIT_COMMENT_BODY);

        // Lets make sure the comment was changed
        gotoIssue("HSP-2");
        assertTextPresent(EDIT_COMMENT_BODY);

        // Make sure we get the exceptions we want when passing bad data

        // Test a null comment
        assertErrorWithWithNullComment();

        // Test a comment with null id
        assertErrorWithNullCommentId();

        // Test a comment with an invalid project role
        assertErrorWithInvalidProjectRole(comment);

        // Test a comment with an invalid group
        assertErrorWithGroupsDisabled(comment);

        // Now lets enable groups and get a better message
        assertErrorWithUnknownGroup(comment);

        // Lets make sure we can't set an empty body
        assertErrorWithEmptyBody(comment);
    }

    public void testEditCommentPermissionErrors() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_edit_comment.xml");
        soapConnect();

        RemoteComment comment = createAndRetrieveCommentOnIssue("HSP-2");

        // Remove the ALL and OWN edit permissions
        removeRolePermission(34, 10002);
        removeRolePermission(35, 10000);

        try
        {
            // Now lets edit the comment
            commentsSoapExerciser.testEditComment(comment, EDIT_COMMENT_BODY);
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().indexOf("Administrator, you do not have the permission to edit this comment.") != -1);
        }

        gotoIssue("HSP-2");
        assertTextNotPresent(EDIT_COMMENT_BODY);
    }

    public void testEditCommentsWithVisibility() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_edit_comment.xml");
        soapConnect();

        RemoteComment comment = createAndRetrieveCommentOnIssue("HSP-2");

        // Now lets edit with visibility
        commentsSoapExerciser.testEditCommentWithVisibility(comment, null, ROLE_ADMINISTRATORS, EDIT_COMMENT_BODY);

        // Lets make sure the comment was changed
        gotoIssue("HSP-2");
        assertTextPresent(EDIT_COMMENT_BODY);

        try
        {
            // Check that we can't see the comment if we are not an Admin
            logout();

            login("user", "user");
            gotoIssue("HSP-2");
            assertTextNotPresent(EDIT_COMMENT_BODY);
        }
        finally
        {
            logout();
            login(ADMIN_USERNAME, ADMIN_PASSWORD);
        }
    }

    public void testHasPermissionToEditComment() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_edit_comment.xml");
        soapConnect();

        RemoteComment comment = createAndRetrieveCommentOnIssue("HSP-2");

        // Now lets see if we can edit this, we should be able to
        boolean hasPerm = commentsSoapExerciser.testHasPermissionToEditComment(comment);

        assertTrue(hasPerm);

        // Remove the ALL and OWN edit permissions
        removeRolePermission(34, 10002);
        removeRolePermission(35, 10000);

        // Now lets ask again, lets hope we don't have the perm
        hasPerm = commentsSoapExerciser.testHasPermissionToEditComment(comment);
        assertFalse(hasPerm);

        try
        {
            commentsSoapExerciser.testHasPermissionToEditComment(null);
            fail();
        }
        catch (RemoteException e)
        {
            // This is what we are after, seems that the message is null, hmmm
            assertTrue(e.toString().indexOf("You can not update a null comment.") != -1);
        }

        try
        {
            RemoteComment remoteComment = new RemoteComment(null, null, null, null, null, null, null, null);
            commentsSoapExerciser.testHasPermissionToEditComment(remoteComment);
            fail();
        }
        catch (RemoteException e)
        {
            // This is what we are after, seems that the message is null, hmmm
            assertTrue(e.toString().indexOf("You can not update a comment with a null id.") != -1);
        }
    }

    private void assertErrorWithEmptyBody(RemoteComment comment)
    {
        try
        {
            comment.setBody("");
            commentsSoapExerciser.testEditCommentAsIs(comment);
            fail();
        }
        catch (RemoteException e)
        {
            // This is what we are after, seems that the message is null, hmmm
            assertTrue(e.toString().indexOf("Comment body can not be empty!") != -1);
        }
    }

    private void assertErrorWithUnknownGroup(RemoteComment comment)
    {
        gotoAdmin();
        clickLink("general_configuration");
        clickLinkWithText("Edit Configuration");
        checkCheckbox("groupVisibility", "true");
        submit("Update");

        // Test a comment with an invalid group
        try
        {
            commentsSoapExerciser.testEditCommentWithVisibility(comment, "kruppa", null, EDIT_COMMENT_BODY);
            fail();
        }
        catch (RemoteException e)
        {
            // This is what we are after, seems that the message is null, hmmm
            assertTrue(e.toString().indexOf("Group: kruppa does not exist.") != -1);
            comment.setGroupLevel(null);
        }
    }

    private void assertErrorWithGroupsDisabled(RemoteComment comment)
    {
        try
        {
            commentsSoapExerciser.testEditCommentWithVisibility(comment, "kruppa", null, EDIT_COMMENT_BODY);
            fail();
        }
        catch (RemoteException e)
        {
            // This is what we are after, seems that the message is null, hmmm
            assertTrue(e.toString().indexOf("Group level visibility has been disabled.") != -1);
        }
    }

    private void assertErrorWithInvalidProjectRole(RemoteComment comment)
    {
        try
        {
            commentsSoapExerciser.testEditCommentWithVisibility(comment, null, "BS_ROLE", EDIT_COMMENT_BODY);
            fail();
        }
        catch (RemoteException e)
        {
            // This is what we are after, seems that the message is null, hmmm
            assertTrue(e.toString().indexOf("Project role: BS_ROLE does not exist") != -1);
        }
    }

    private void assertErrorWithNullCommentId()
    {
        try
        {
            RemoteComment remoteComment = new RemoteComment(null, null, null, null, null, null, null, null);
            commentsSoapExerciser.testEditCommentAsIs(remoteComment);
            fail();
        }
        catch (RemoteException e)
        {
            // This is what we are after, seems that the message is null, hmmm
            assertTrue(e.toString().indexOf("You can not update a comment with a null id.") != -1);
        }
    }

    private void assertErrorWithWithNullComment()
    {
        try
        {
            commentsSoapExerciser.testEditCommentAsIs(null);
            fail();
        }
        catch (RemoteException e)
        {
            // This is what we are after, seems that the message is null, hmmm
            assertTrue(e.toString().indexOf("You can not update a null comment.") != -1);
        }
    }

    private RemoteComment createAndRetrieveCommentOnIssue(String issueKey) throws RemoteException
    {
        commentsSoapExerciser.testAddComment(issueKey, NEW_COMMENT_BODY);

        // Get the comment we just added
        RemoteComment[] comments = commentsSoapExerciser.testGetComments(issueKey);

        assertEquals("There should be only one comment", 1, comments.length);

        // Test the getComment method
        return commentsSoapExerciser.testGetComment(new Long(comments[0].getId()));
    }

    private void assertCommentContent(RemoteComment comment, String expectedBody, String expectedGroupLevel, String expectedRoleLevel)
    {
        assertEquals(expectedBody, comment.getBody());
        assertEquals(expectedGroupLevel, comment.getGroupLevel());
        assertEquals(expectedRoleLevel, comment.getRoleLevel());
    }

}
