package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteAttachment;
import com.atlassian.jira.rpc.soap.client.RemoteConfiguration;
import com.atlassian.jira.rpc.soap.client.RemoteIssue;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Iterables;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;

public final class TestAttachmentsSoapExerciser extends SOAPTestCase
{
    private void enableAttachments() throws IOException, SAXException
    {
        RemoteConfiguration jiraConfiguration = jiraAdminSoapExerciser.testGetRemoteConfiguration();
        if (!jiraConfiguration.isAllowAttachments())
        {
            getAdministration().attachments().enable();
        }
    }

    public void testAttachFileToIssue() throws IOException, SAXException
    {
        enableAttachments();

        RemoteIssue issue = issueSoapExerciser.testGetIssueById(ISSUE_ID);
        String fileName = attachmentsSoapExerciser.testAddAttachment(issue);
        gotoIssue(ISSUE_KEY);
        assertTextPresent(fileName);
        issue = issueSoapExerciser.testGetIssueById(ISSUE_ID);
        // validate that the attachments returned in the issue have the correct information
        assertEquals(1, issue.getAttachmentNames().length);
        assertEquals(fileName, issue.getAttachmentNames()[0]);
        // validate that the change history was added for the attachment
        assertLastChangeHistoryIs(ISSUE_KEY, ATTACHMENT_FIELD_ID, "", fileName);

        //get the issue again and check that the attachment is part of it
        RemoteIssue issueAfterAttachment = issueSoapExerciser.testGetIssueById(ISSUE_ID);
        String[] attachmentNames = issueAfterAttachment.getAttachmentNames();
        assertEquals(1, attachmentNames.length);
        assertEquals(fileName, attachmentNames[0]);
        RemoteAttachment[] remoteAttachments = attachmentsSoapExerciser.testGetAttachments(ISSUE_KEY);
        assertEquals(1, remoteAttachments.length);
        assertEquals(fileName, remoteAttachments[0].getFilename());
        assertEquals(LOGIN_NAME, remoteAttachments[0].getAuthor());
    }

    public void testAttachFilesToIssueUsingBase64() throws IOException, SAXException
    {
        enableAttachments();

        // Add attachments to the issue
        RemoteIssue initialIssue = issueSoapExerciser.testGetIssueById(ISSUE_ID);
        final List<String> createdAttachmentFileNames =
                Arrays.asList(attachmentsSoapExerciser.testAddMultipleAttachmentsUsingBase64(initialIssue));

        // Load a new RemoteIssue instance which should have the file names of the attachments we added
        RemoteIssue updatedIssue = issueSoapExerciser.testGetIssueById(ISSUE_ID);

        // Verify that the file names returned in the issue match the names of the attachments we added
        assertEquals(
                HashMultiset.create(createdAttachmentFileNames),
                HashMultiset.create(Arrays.asList(updatedIssue.getAttachmentNames()))
        );

        // Navigate to the issue in JIRA and assert that all the attachments which were created can be seen in the
        // view issue page
        gotoIssue(ISSUE_KEY);
        for (String fileName : createdAttachmentFileNames)
        {
            assertTextPresent(fileName);
        }

        // Get the RemoteAttachments for the issue and check that the attachments we added are part of it
        final Iterable<RemoteAttachment> remoteAttachments =
                HashMultiset.create(Arrays.asList(attachmentsSoapExerciser.testGetAttachments(ISSUE_KEY)));

        assertTrue(Iterables.size(remoteAttachments) > 0);

        // Assert that the file names of the returned remoteAttachments match the file names of the attachments we added
        assertEquals(HashMultiset.create(createdAttachmentFileNames),
                HashMultiset.create(Iterables.transform(remoteAttachments, new Function<RemoteAttachment, String>()
                {
                    public String apply(@Nullable final RemoteAttachment remoteAttachment)
                    {
                        return remoteAttachment.getFilename();
                    }
                })));

        // Assert that the author of all the returned remoteAttachments matches the user we logged in with
        assertTrue(
                Iterables.all(remoteAttachments, new Predicate<RemoteAttachment>()
                {
                    public boolean apply(@Nullable final RemoteAttachment remoteAttachment)
                    {
                        return remoteAttachment.getAuthor().equals(LOGIN_NAME);
                    }
                }));
    }

    public void testAttachLargeFileToIssueUsingBase64() throws IOException, SAXException
    {
        final int BIG_FILE_SIZE = 1500000;
        enableAttachments();

        RemoteIssue issue = issueSoapExerciser.testGetIssueById(ISSUE_ID);
        String fileName = attachmentsSoapExerciser.testAddLargeAttachmentUsingBase64(issue, BIG_FILE_SIZE);

        gotoIssue(ISSUE_KEY);
        issue = issueSoapExerciser.testGetIssueById(ISSUE_ID);
        // validate that the attachments returned in the issue have the correct information
        assertEquals(1, issue.getAttachmentNames().length);
        assertTextPresent(fileName);
        assertEquals(fileName, issue.getAttachmentNames()[0]);

        // try to view the attachment and ensure the content length
        clickLinkWithText(fileName);
        final int webFileSize = getDialog().getResponse().getContentLength();
        assertEquals("Actual file size from requesting attachment via web (" + webFileSize + ") differs from expected size (" + BIG_FILE_SIZE + ")", BIG_FILE_SIZE, webFileSize);

        //get the issue again and check that the attachment is part of it
        RemoteAttachment[] remoteAttachments = attachmentsSoapExerciser.testGetAttachments(ISSUE_KEY);
        assertEquals(1, remoteAttachments.length);
        assertEquals(fileName, remoteAttachments[0].getFilename());
        assertEquals(LOGIN_NAME, remoteAttachments[0].getAuthor());
        final Long soapFileSize = remoteAttachments[0].getFilesize();
        assertEquals("Actual file size from requesting attachment via SOAP (" + soapFileSize + ") differs from expected size (" + BIG_FILE_SIZE + ")", new Long(BIG_FILE_SIZE), soapFileSize);
    }
}
