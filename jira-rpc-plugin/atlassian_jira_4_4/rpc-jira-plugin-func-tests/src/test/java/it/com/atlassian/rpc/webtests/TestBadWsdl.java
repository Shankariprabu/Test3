package it.com.atlassian.rpc.webtests;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Make sure that a bad SOAP call cannot corrupt the JIRA WSDL.
 *
 * Ref: http://jira.atlassian.com/browse/JRA-20351
 * Ref: https://jdog.atlassian.com/browse/JRADEV-3308
 *
 * @since v4.2
 */
public class TestBadWsdl extends SOAPTestCase
{
    private static final Pattern BAD_WSDL_PATTERN = Pattern.compile("complexType.*String");
    private static final String BAD_STRING_MSG = "SimpleDeserializer encountered a child element, which is NOT expected, in something it was trying to deserialize.";

    private static final String MIME_XML = "application/xml";
    private static final String CHARSET_UTF8 = "UTF-8";

    private static final String HEADER_USER_AGENT = "User-Agent";
    private static final String HEADER_SOAP_ACTION = "SOAPAction";
    private static final String HEADER_ACCEPT = "Accept";

    public void testGenerateBadWSDL() throws Exception
    {
        File badFile = new File(getEnvironmentData().getXMLDataLocation(), "badrequest.xml");
        String badRequestString = FileUtils.readFileToString(badFile, CHARSET_UTF8);
        badRequestString = badRequestString.replace("${authToken}", Matcher.quoteReplacement(getToken()));

        HttpClient client = new HttpClient();
        assertGoodWsdl(client);

        PostMethod method = new PostMethod(getServiceURL());
        method.addRequestHeader(HEADER_USER_AGENT, "Axis/1.3");
        method.addRequestHeader(HEADER_SOAP_ACTION, "\"\"");
        method.addRequestHeader(HEADER_ACCEPT, MIME_XML);
        method.setRequestEntity(new StringRequestEntity(badRequestString, MIME_XML, CHARSET_UTF8));

        try
        {
            int statusCode = client.executeMethod(method);
            assertEquals("Expecting 500 reply but got " + statusCode + "(" + HttpStatus.getStatusText(statusCode) + ") instead.",
                    HttpStatus.SC_INTERNAL_SERVER_ERROR, statusCode);
            String response = method.getResponseBodyAsString();
            assertTrue("The response did not contain the correct error message.", response.contains(BAD_STRING_MSG));
        }
        finally
        {
            method.releaseConnection();
        }

        assertGoodWsdl(client);
    }

    private void assertGoodWsdl(final HttpClient client) throws IOException
    {
        GetMethod method = new GetMethod(getServiceURL() + "?wsdl");
        int statusCode = client.executeMethod(method);
        try
        {
            assertEquals("Unable to get WSDL " + HttpStatus.getStatusText(statusCode), HttpStatus.SC_OK, statusCode);
            assertTrue("WSDL contain string as complex type. This is bad.", !BAD_WSDL_PATTERN.matcher(method.getResponseBodyAsString()).find());
        }
        finally
        {
            method.releaseConnection();
        }
    }
}
