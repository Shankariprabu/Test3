package it.com.atlassian.rpc.bootstrap;

import com.atlassian.jira.webtests.JIRAWebTest;

/**
 * DO NOT RUN THIS TEST VIA THE AcceptanceTestHarness
 * This is used so that we can run it to setup JIRA and
 * import an xml backup that will allow us to enable the
 * RPC plugin. We use this in the nightly build so that we can
 * get the WSDL before we run the main func tests.
 */
public class BootstrapJIRAEnableRPC extends JIRAWebTest
{
    public BootstrapJIRAEnableRPC(String name)
    {
        super(name);
    }

    public void setUp()
    {
        super.setUp();
        restoreData("BootstrapJIRAWithRPCEnabled.xml");
    }

    // NOTE: this does nothing, this is here just to bootstrap JIRA.
    public void testNothing()
    {
    }
}

