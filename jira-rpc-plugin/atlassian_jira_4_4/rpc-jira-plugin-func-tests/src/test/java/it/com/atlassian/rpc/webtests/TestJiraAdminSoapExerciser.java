package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteConfiguration;
import com.atlassian.jira.rpc.soap.client.RemotePermissionException;
import com.atlassian.jira.rpc.soap.client.RemoteServerInfo;

import java.rmi.RemoteException;

/**
 *
 */
public class TestJiraAdminSoapExerciser extends SOAPTestCase
{
    public void testGetRemoteServerInfo() throws RemoteException
    {
        RemoteServerInfo serverInfo = jiraAdminSoapExerciser.testGetRemoteServerInfo();
        assertNotNull(serverInfo.getBaseUrl());
        assertNotNull(serverInfo.getBuildDate());
        assertNotNull(serverInfo.getBuildNumber());
    }

    public void testGetRemoteConfiguration() throws RemoteException
    {
        RemoteConfiguration config = jiraAdminSoapExerciser.testGetRemoteConfiguration();
        assertFalse(config.isAllowAttachments());
        assertFalse(config.isAllowIssueLinking());
        assertTrue(config.isAllowUnassignedIssues());
    }

    public void testIssueTypes() throws RemoteException
    {
        restoreData("jira_soap_client_func_test2.xml");
        // Try to get the issue types as admin
        assertEquals(4, jiraAdminSoapExerciser.getIssueTypes(LOGIN_NAME, LOGIN_PASSWORD).length);

        try
        {
            // Try to get the issue types as fred who does not have permission to do so
            jiraAdminSoapExerciser.getIssueTypes("fred", "fred");
            fail();
        }
        catch (RemotePermissionException rpe)
        {
            // This should happen :)
        }
    }

    public void testSubtaskIssueTypes() throws RemoteException
    {
        restoreData("jira_soap_client_func_test2.xml");
        // Try to get the issue types as admin
        assertEquals(1, jiraAdminSoapExerciser.getSubTaskIssueTypes(LOGIN_NAME, LOGIN_PASSWORD).length);

        try
        {
            // Try to get the issue types as fred who does not have permission to do so
            jiraAdminSoapExerciser.getSubTaskIssueTypes("fred", "fred");
            fail();
        }
        catch (RemotePermissionException rpe)
        {
            // This should happen :)
        }
    }

    public void testPriorities() throws RemoteException
    {
        restoreData("jira_soap_client_func_test2.xml");
        // Try to get the issue types as admin
        assertEquals(5, jiraAdminSoapExerciser.getPriorities(LOGIN_NAME, LOGIN_PASSWORD).length);

        try
        {
            // Try to get the issue types as fred who does not have permission to do so
            jiraAdminSoapExerciser.getPriorities("fred", "fred");
            fail();
        }
        catch (RemotePermissionException rpe)
        {
            // This should happen :)
        }
    }

    public void testResolutions() throws RemoteException
    {
        restoreData("jira_soap_client_func_test2.xml");
        // Try to get the issue types as admin
        assertEquals(5, jiraAdminSoapExerciser.getResolutions(LOGIN_NAME, LOGIN_PASSWORD).length);

        try
        {
            // Try to get the issue types as fred who does not have permission to do so
            jiraAdminSoapExerciser.getResolutions("fred", "fred");
            fail();
        }
        catch (RemotePermissionException rpe)
        {
            // This should happen :)
        }
    }

    public void testStatuses() throws RemoteException
    {
        restoreData("jira_soap_client_func_test2.xml");
        // Try to get the issue types as admin
        assertEquals(5, jiraAdminSoapExerciser.getStatuses(LOGIN_NAME, LOGIN_PASSWORD).length);

        try
        {
            // Try to get the issue types as fred who does not have permission to do so
            jiraAdminSoapExerciser.getStatuses("fred", "fred");
            fail();
        }
        catch (RemotePermissionException rpe)
        {
            // This should happen :)
        }
    }
}