package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteException;
import com.atlassian.jira.rpc.soap.client.RemotePermissionException;
import com.atlassian.jira.rpc.soap.client.RemoteSecurityLevel;

import java.util.Arrays;

public class TestSecurityLevel extends SOAPTestCase
{

    public void testBrowseButNotSecurityLevelPermission() throws Exception
    {
        restoreData("jira_soap_client_func_test_create_issue_with_security.xml");

        soapConnect();
        final RemoteSecurityLevel[] remoteSecurityLevels = soapSession.getJiraSoapService().getSecurityLevels(getToken(), "TST");
        assertEquals(2, remoteSecurityLevels.length);

        soapConnect("fred", "fred");
        final RemoteSecurityLevel[] levels = soapSession.getJiraSoapService().getSecurityLevels(getToken(), "TST");
        assertEquals(0, levels.length);
    }

    public void testSecurityLevelsWithBrowseProjectPermissions() throws Exception
    {
        restoreData("jira_soap_client_func_test_create_issue_with_security.xml");
        soapConnect("fred", "fred");

        // Fred cannot see MKY project
        try
        {
            soapSession.getJiraSoapService().getSecurityLevels(getToken(), "MKY");
            fail();
        }
        catch (RemoteException e)
        {
            // expected
            assertTrue(e.getFaultString().contains("You must have the browse project permission to view this project"));
        }

        // Fred can see HSP project
        RemoteSecurityLevel[] levels = soapSession.getJiraSoapService().getSecurityLevels(getToken(), "HSP");
        assertEquals(0, levels.length);
    }

    public void testGetSecurityLevelsInvalidProject() throws Exception
    {
        restoreData("jira_soap_client_func_test_create_issue_with_security.xml");
        soapConnect();
        try
        {
            soapSession.getJiraSoapService().getSecurityLevels(getToken(), "XYZ");
            fail();
        }
        catch (RemoteException e)
        {
            // expected
            assertTrue(e.getFaultString().contains("Error retrieving project with key 'XYZ': No project could be found with key 'XYZ'. "));
        }
    }

    public void testGetSecurityLevels() throws Exception
    {
        restoreData("jira_soap_client_func_test_create_issue_with_security.xml");
        soapConnect();

        RemoteSecurityLevel[] levels = soapSession.getJiraSoapService().getSecurityLevels(getToken(), "MKY");
        assertEquals(2, levels.length);
        assertContains(new RemoteSecurityLevel("10000", "Everybody", "Short desc"), levels);
        assertContains(new RemoteSecurityLevel("10001", "Limited", "Longer description"), levels);

        levels = soapSession.getJiraSoapService().getSecurityLevels(getToken(), "HSP");
        assertEquals(0, levels.length);
    }

    public void testNoSecurityLevel() throws Exception
    {
        restoreData("jira_soap_client_func_test_create_issue_with_security.xml");
        soapConnect();
        assertNull(soapSession.getJiraSoapService().getSecurityLevel(getToken(), "HSP-1"));
    }

    public void testNoSuchIssue() throws Exception
    {
        restoreData("jira_soap_client_func_test_create_issue_with_security.xml");
        soapConnect();
        try
        {
            soapSession.getJiraSoapService().getSecurityLevel(getToken(), "HSP-100");
            fail();
        }
        catch (RemotePermissionException e)
        {
            // expected
            assertMessageContains(e, "This issue does not exist.");
        }

    }

    private void assertContains(RemoteSecurityLevel level, RemoteSecurityLevel[] a)
    {
        assertTrue(Arrays.asList(a).contains(level));
    }

    private void assertMessageContains(final RemotePermissionException e, final String msg)
    {
        assertTrue(e.toString().contains(msg));
    }
}
