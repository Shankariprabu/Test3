package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.admin.GeneralConfiguration;
import com.atlassian.jira.rpc.soap.client.JiraSoapService;
import com.atlassian.jira.rpc.soap.client.RemoteAuthenticationException;
import com.atlassian.jira.rpc.soap.client.RemoteGroup;
import com.atlassian.jira.rpc.soap.client.RemotePermissionException;
import com.atlassian.jira.rpc.soap.client.RemoteProject;
import com.atlassian.jira.rpc.soap.client.RemoteProjectRole;
import com.atlassian.jira.rpc.soap.client.RemoteProjectRoleActors;
import com.atlassian.jira.rpc.soap.client.RemoteRoleActors;
import com.atlassian.jira.rpc.soap.client.RemoteScheme;
import com.atlassian.jira.rpc.soap.client.RemoteUser;
import com.atlassian.jira.rpc.soap.client.RemoteValidationException;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.ProjectRole;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.ProjectRoleClient;
import com.atlassian.jira_soapclient.exercise.ExerciserClientConstants;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.webtests.Groups.ADMINISTRATORS;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;

/**
 *
 */
public class TestUserAdminSoapExerciser extends SOAPTestCase implements FunctTestConstants
{
    protected static final String ATLASSIAN_USER_ROLE_ACTOR = "atlassian-user-role-actor";

    protected static final String UPDATED_DESCRIPTION = "updated description";

    protected static final String GROUP_JIRA_DEVELOPERS = "jira-developers";
    protected static final String GROUP_JIRA_USERS = "jira-users";

    protected static final String ADMIN_GROUP_2 = "jira-admin-2";

    protected static final String ADMIN_USER = "admin";
    protected static final String OTHER_GROUP = "jira-developers";
    protected static final String ADMIN_GROUP = "jira-administrators";

    public static final java.lang.String ERROR_LEAVING_ALL_ADMIN_GROUPS = "You are trying to leave all of the administration groups jira-administrators. You cannot delete your own administration permission";
    public static final java.lang.String ERROR_LEAVING_ALL_SYS_ADMIN_GROUPS = "You are trying to leave all of the system administration groups jira-administrators. You cannot delete your own system administration permission";

    // JRA-19498
    public void testEmailAddressVisibility() throws Exception
    {
        restoreData("jira_soap_client_func_test_create_project.xml");
        soapConnect();

        _testRemoteUserEmailAddress(GeneralConfiguration.EmailVisibility.PUBLIC, "dev@dev.com");
        _testRemoteUserEmailAddress(GeneralConfiguration.EmailVisibility.LOGGED_IN_ONLY, "dev@dev.com");
        _testRemoteUserEmailAddress(GeneralConfiguration.EmailVisibility.MASKED, "dev at dev dot com");
        _testRemoteUserEmailAddress(GeneralConfiguration.EmailVisibility.HIDDEN, null);
    }

    private void _testRemoteUserEmailAddress(final GeneralConfiguration.EmailVisibility visibility, final String expectedEmail) throws RemoteException
    {
        administration.generalConfiguration().setUserEmailVisibility(visibility);
        RemoteUser remoteUser = soapSession.getJiraSoapService().getUser(getToken(), "dev");
        assertEquals(expectedEmail, remoteUser.getEmail());

        RemoteGroup remoteGroup = soapSession.getJiraSoapService().getGroup(getToken(), "jira-users");
        for (RemoteUser user : remoteGroup.getUsers())
        {
            switch (visibility)
            {
                case LOGGED_IN_ONLY:
                case PUBLIC:
                    assertTrue(user.getEmail().contains("@"));
                    assertTrue(user.getEmail().contains(".com"));
                    break;
                case MASKED:
                    assertTrue(user.getEmail().contains(" at "));
                    assertTrue(user.getEmail().contains(" dot "));
                    break;
                case HIDDEN:
                    assertNull(user.getEmail());
                    break;
            }
        }
    }

    public void testDeleteGroupCantDeleteLastAdmin() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_create_project.xml");
        soapConnect();

        try
        {
            userAdminSoapExerciser.testDeleteGroup("jira-administrators", null);
            fail();
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You cannot delete a group that grants you system administration privileges if no other group exists that also grants you system administration privileges."));
        }
    }

    public void testDeleteGroup() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_create_project.xml");
        soapConnect();

        userAdminSoapExerciser.testDeleteGroup("jira-developers", null);
        gotoGroupBrowser();
        assertTextNotPresent("jira-developers");
    }

    public void testDeleteGroupNoPerms() throws RemoteException
    {
        try
        {
            soapConnect(FRED_USERNAME, FRED_PASSWORD);
            userAdminSoapExerciser.testDeleteGroup("jira-developers", null);
            fail("Fred should not have permission to do anything.");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You must be at least a JIRA Administrator to manipulate a group."));
        }
        gotoGroupBrowser();
        assertTextPresent("jira-developers");
    }

    public void testDeleteInvalidGroup() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.testDeleteGroup("invalid", null);
            fail("Group does not exist, should not be able to delete it");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("The group 'invalid' is not a valid group."));
        }
    }

    public void testDeleteGroupExternalUserManagementEnabled() throws RemoteException
    {
        try
        {
            toggleExternalUserManagement(true);
            userAdminSoapExerciser.testDeleteGroup(DEVELOPERS, null);
            fail("Should not be able to delete it");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("Error validating group deletion. Cannot delete group, as external user management is enabled, please contact your JIRA administrators."));
        }
    }

    public void testTwoAdminGroupsUserIsMemberOfOne() throws RemoteException
    {
        //Setup test data for TC3&4 (Two admin groups, user is a member of one)
        restoreData("jira_soap_client_func_test_create_project.xml");
        createGroup(ADMIN_GROUP_2);
        grantGlobalPermission(SYSTEM_ADMINISTER, ADMIN_GROUP_2);

        //Test Case 3: User attempts to delete the admin group they are a part of.
        try
        {
            soapConnect();
            userAdminSoapExerciser.testDeleteGroup(ADMIN_GROUP, null);
            fail();
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You cannot delete a group that grants you system administration privileges if no other group exists that also grants you system administration privileges."));
        }

        //Test Case 4: User then attempts to delete the admin group they aren't a part of.
        userAdminSoapExerciser.testDeleteGroup(ADMIN_GROUP_2, null);

        gotoGroupBrowser();
        assertTextNotPresent(ADMIN_GROUP_2);
    }

    public void testTwoSysAdminGroupsUserIsMemberOfBoth() throws RemoteException
    {
        //Setup test data for TC5&6 (Two admin groups, user is a member of both)
        restoreData("jira_soap_client_func_test_create_project.xml");
        createGroup(ADMIN_GROUP_2);
        grantGlobalPermission(SYSTEM_ADMINISTER, ADMIN_GROUP_2);
        addUserToGroup(ADMIN_USER, ADMIN_GROUP_2);

        //Test Case 5: User attempts to delete one of their admin groups.
        soapConnect();
        userAdminSoapExerciser.testDeleteGroup(ADMIN_GROUP, null);
        gotoGroupBrowser();
        assertTextNotPresent(ADMIN_GROUP);

        //Test Case 6: User attempts to delete their other admin group.
        try
        {
            userAdminSoapExerciser.testDeleteGroup(ADMIN_GROUP_2, null);
            fail("should have caught ex");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You cannot delete a group that grants you system administration privileges if no other group exists that also grants you system administration privileges."));
        }
    }

    public void testNoSysAdminGroupOneAdminGroupOneOther() throws RemoteException
    {
        try
        {
            //Setup test data for TC1&2 (One admin group, user is a member)
            restoreData("jira_soap_client_func_test_sys_admin.xml");

            //Test Case 1: User attempts to delete the admin group (they are a member).
            try
            {
                soapConnect();
                userAdminSoapExerciser.testDeleteGroup(ADMIN_GROUP, null);
                fail();
            }
            catch (RemoteException e)
            {
                assertTrue(e.toString().contains("You cannot delete a group that grants you administration privileges if no other group exists that also grants you administration privileges."));
            }

            //Test Case 2: User then attempts to delete a different (non-admin) group they aren't a part of.
            userAdminSoapExerciser.testDeleteGroup(OTHER_GROUP, null);
            gotoGroupBrowser();
            assertTextNotPresent(OTHER_GROUP);
        }
        finally
        {
            logout();
            // go back to sysadmin user
            login("root", "root");
            restoreData("jira_soap_client_func_test_create_project.xml");
        }
    }

    public void testNoSysAdminGroupTwoAdminGroupUserIsMemberOfBoth() throws RemoteException
    {
        try
        {
            //Setup test data for TC1&2 (One admin group, user is a member)
            restoreData("jira_soap_client_func_test_sys_admin.xml");

            createGroup(ADMIN_GROUP_2);
            giveAdminPermission(ADMIN_GROUP_2);
            addUserToGroup(ADMIN_USER, ADMIN_GROUP_2);

            //Test Case 5: User attempts to delete one of their admin groups.
            soapConnect();
            userAdminSoapExerciser.testDeleteGroup(ADMIN_GROUP, null);
            gotoGroupBrowser();
            assertTextNotPresent(ADMIN_GROUP);

            //Test Case 6: User attempts to delete their other admin group.
            try
            {
                userAdminSoapExerciser.testDeleteGroup(ADMIN_GROUP_2, null);
                fail();
            }
            catch (RemoteException e)
            {
                assertTrue(e.toString().contains("You cannot delete a group that grants you administration privileges if no other group exists that also grants you administration privileges."));
            }
        }
        finally
        {
            logout();
            // go back to sysadmin user
            login("root", "root");
            restoreData("jira_soap_client_func_test_create_project.xml");
        }
    }

    public void testDeleteSysAdminGroupAsAdmin() throws RemoteException
    {
        try
        {
            //Setup test data for TC1&2 (One admin group, user is a member)
            restoreData("jira_soap_client_func_test_sys_admin.xml");

            // Try to delete a group we should not have permission to delete
            try
            {
                soapConnect();
                userAdminSoapExerciser.testDeleteGroup("jira-sys-admins", null);
                fail();
            }
            catch (RemoteException e)
            {
                assertTrue(e.toString().contains("Cannot delete group, only System Administrators can delete groups associated with the System Administrators global permission."));
            }
        }
        finally
        {
            logout();
            // go back to sysadmin user
            login("root", "root");
            restoreData("jira_soap_client_func_test_create_project.xml");
        }
    }

    public void testDeleteGroupSwapGroup() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_swap_group.xml");

        gotoIssue("HSP-1");
        assertTextPresent("Test Comment Visibility");

        soapConnect();
        userAdminSoapExerciser.testDeleteGroup(ADMINISTRATORS, "other-admins");

        gotoIssue("HSP-1");
        assertTextPresent("Test Comment Visibility");
    }

    public void testDeleteGroupSwapGroupErrorNoSwap() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_swap_group.xml");

        gotoIssue("HSP-1");
        assertTextPresent("Test Comment Visibility");

        try
        {
            soapConnect();
            userAdminSoapExerciser.testDeleteGroup(ADMINISTRATORS, null);
            fail();
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You must specify a group to move comments/worklogs to."));
        }
    }

    public void testDeleteGroupSwapGroupErrorSameGroup() throws RemoteException
    {
        restoreData("jira_soap_client_func_test_swap_group.xml");

        gotoIssue("HSP-1");
        assertTextPresent("Test Comment Visibility");

        try
        {
            soapConnect();
            userAdminSoapExerciser.testDeleteGroup(ADMINISTRATORS, ADMINISTRATORS);
            fail();
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You cannot swap comments/worklogs to the group you are deleting."));
        }
    }

    public void testAdminCannotDeleteSysadmin() throws RemoteException
    {
        try
        {
            restoreData("jira_soap_client_func_test_sys_admin.xml");

            try
            {
                soapConnect();
                userAdminSoapExerciser.testDeleteUser(SYS_ADMIN_USERNAME);
            }
            catch (RemoteException e)
            {
                assertTrue(e.toString().contains("As a user with JIRA Administrators permission, you cannot delete users with JIRA System Administrators permission."));
            }

            gotoAdmin();
            clickLink("user_browser");
            assertLinkPresent(SYS_ADMIN_USERNAME); // check user is still in user list
        }
        finally
        {
            logout();
            // go back to sysadmin user
            login("root", "root");
            restoreData("jira_soap_client_func_test_create_project.xml");
        }
    }

    private void attemptToDeleteGroup(String groupName)
    {
        gotoGroupBrowser();
        String linkId = "del_" + groupName;
        clickLink(linkId);
    }

    private void giveAdminPermission(String groupName)
    {
        gotoAdmin();
        clickLink("global_permissions");
        selectOption("permType", "JIRA Administrators");
        selectOption("groupName", groupName);
        submit("Add");
    }

    private void gotoGroupBrowser()
    {
        gotoAdmin();
        clickLink("group_browser");
    }

    public void testCreateUser() throws RemoteException
    {
        List<String> expectedGroups = new ArrayList<String>();
        expectedGroups.add(USERS);
        createrUserAndAssert("newusername1", "password1", "newUsersFullName1", "newUsers1@email.com", expectedGroups);

        // add jira-developers to the global users permission so that newly created users are a member of it
        clickOnAdminPanel("admin.globalsettings", "global_permissions");
        selectOption("permType", "JIRA Users");
        selectOption("groupName", DEVELOPERS);
        submit("Add");

        // create a new user and check they are a member of jira-developers and jira-users.
        expectedGroups.add(DEVELOPERS);

        // create a user with leading and trailing spaces in their name
        createrUserAndAssert("  newusername3  ", "password3", "newUsersFullName3", "newUsers3@email.com", expectedGroups);

        // now that we have a new user lets edit him and assert that it works
        editUserDetailsAndAssert("newusername1", "passwordUpdated", "fullNameUpdated", "emailUpdated@email.com");

    }

    private void createrUserAndAssert(String username, String password, String fullname, String email, Collection expectedGroups)
            throws RemoteException
    {
        // add the user
        RemoteUser remoteUser = userAdminSoapExerciser.testAddUser(username, password, fullname, email);
        assertEquals(username.trim(), remoteUser.getName());
        assertEquals(fullname, remoteUser.getFullname());
        assertEquals(email, remoteUser.getEmail());

        //check that the user is in the expected groups
        assertUserIsMemberOfGroups(username.trim(), expectedGroups);
    }

    private void editUserDetailsAndAssert(String userName, String password, String fullName, String email) throws RemoteException
    {
        RemoteUser remoteUser =  userAdminSoapExerciser.updateUser(userName, fullName, email);
        assertEquals(userName.trim(), remoteUser.getName());
        assertEquals(fullName, remoteUser.getFullname());
        assertEquals(email, remoteUser.getEmail());

        assertUserDetailsViaTheWeb(userName, fullName, email);


        userAdminSoapExerciser.updateUserPassword(userName, password);

        loginAsTheBackToAdmin(userName, password);

        // ok the user should be able update their own details
        try
        {
            JiraSoapService jiraSoapService = soapSession.getJiraSoapService();
            String userToken = jiraSoapService.login(userName, password);

            remoteUser.setEmail("updatedbyhimself@example.com");
            remoteUser.setFullname("updatedbyhimself");

            RemoteUser selfUpdatedUser = jiraSoapService.updateUser(userToken, remoteUser);

            assertEquals(selfUpdatedUser.getName(), remoteUser.getName());
            assertEquals(selfUpdatedUser.getFullname(), remoteUser.getFullname());
            assertEquals(selfUpdatedUser.getEmail(), remoteUser.getEmail());

            assertUserDetailsViaTheWeb(remoteUser.getName(), remoteUser.getFullname(), remoteUser.getEmail());

            String secretnewpassword = "secretnewpassword";
            jiraSoapService.setUserPassword(userToken, selfUpdatedUser, secretnewpassword);

            loginAsTheBackToAdmin(selfUpdatedUser.getName(), secretnewpassword);

        }
        catch (RemoteException e)
        {
            fail("Cant connect to soap as a specific user : " + userName);
        }

    }

    private void assertUserDetailsViaTheWeb(String userName, String fullName, String email)
    {
        tester.gotoPage("secure/ViewProfile.jspa?name=" + userName);

        text.assertTextSequence(locator.css("#details-profile-fragment"),
                "Username:",userName,
                "Full Name:", fullName,
                "Email:", email);
    }

    private void loginAsTheBackToAdmin(String userName, String password)
    {
        try
        {
            navigation.login(userName,password);
        }
        finally
        {
            navigation.login(ADMIN_USER);
        }
    }

    public void testUpdateGroup() throws RemoteException
    {
        userAdminSoapExerciser.testUpdateGroup("jira-developers", new String[] { "fred" });

        gotoPage("/secure/admin/user/GroupBrowser.jspa");
        assertLinkPresentWithText(GROUP_JIRA_DEVELOPERS);
        clickLinkWithText(GROUP_JIRA_DEVELOPERS);
        clickLink("view_group_members");
        assertTextPresent(FRED_USERNAME);
        assertLinkNotPresent("deleteuser_link_" + ADMIN_USERNAME);
        assertLinkPresent("deleteuser_link_" + FRED_USERNAME);
    }

    public void testUpdateGroupAdminNotSysAdmin() throws RemoteException
    {
        // test add to admin group
        userAdminSoapExerciser.testUpdateGroup(ADMINISTRATORS, new String[] { FRED_USERNAME, ADMIN_USERNAME });
        gotoGroupBrowser();
        clickLinkWithText(ADMINISTRATORS);
        clickLink("view_group_members");
        assertTextPresent(FRED_USERNAME);
        assertTextPresent(ADMIN_USERNAME);
        assertLinkPresent("deleteuser_link_" + FRED_USERNAME);
        assertLinkPresent("deleteuser_link_" + ADMIN_USERNAME);

        // now remove from admin group
        userAdminSoapExerciser.testUpdateGroup(ADMINISTRATORS, new String[] { ADMIN_USERNAME });
        gotoGroupBrowser();
        clickLinkWithText(ADMINISTRATORS);
        clickLink("view_group_members");
        assertTextNotPresent(FRED_USERNAME);
        assertTextPresent(ADMIN_USERNAME);
        assertLinkNotPresent("deleteuser_link_" + FRED_USERNAME);
        assertLinkPresent("deleteuser_link_" + ADMIN_USERNAME);
    }

    public void testUpdateGroupExternalUserManagement() throws RemoteException
    {
        toggleExternalUserManagement(true);
        try
        {
            userAdminSoapExerciser.testUpdateGroup(ADMINISTRATORS, new String[] { FRED_USERNAME, ADMIN_USERNAME });
            fail("expected exception");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("Cannot edit group memberships, as external user management is enabled, please contact your JIRA administrators."));
        }
    }

    public void testUpdateGroupInvalidUser()
    {
        try
        {
            userAdminSoapExerciser.testUpdateGroup(ADMINISTRATORS, new String[] { "invalid" });
        }
        catch (RemoteException e)
        {
            assertTrue("Actual: [" + e.toString() + "]", e.toString().contains("Cannot add user. 'invalid' does not exist"));
        }
    }

    public void testUpdateGroupGroupDoesNotExist() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.testUpdateGroup("invalid", new String[] { ADMIN_USERNAME });
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("group cannot be updated, because it doesn't exist."));
        }
    }

    public void testUpdateGroupRemoveLastSysAdminGroup()
    {
        try
        {
            userAdminSoapExerciser.testUpdateGroup(ADMINISTRATORS, new String[] { });
            fail("expected exception");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains(ERROR_LEAVING_ALL_SYS_ADMIN_GROUPS));
        }
    }

    public void testRemoveUserFromGroup() throws RemoteException
    {
        userAdminSoapExerciser.testRemoveUserFromGroup(GROUP_JIRA_DEVELOPERS, ADMIN_USERNAME);
        gotoPage("/secure/admin/user/GroupBrowser.jspa");
        assertLinkPresentWithText(GROUP_JIRA_DEVELOPERS);
        clickLinkWithText(GROUP_JIRA_DEVELOPERS);
        clickLink("view_group_members");
        assertLinkNotPresent("deleteuser_link_" + ADMIN_USERNAME);
    }

    public void testRemoveUserFromGroupRemoveLastSysAdminGroup()
    {
        try
        {
            userAdminSoapExerciser.testRemoveUserFromGroup(ADMINISTRATORS, ADMIN_USERNAME);
            fail("expected exception");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains(ERROR_LEAVING_ALL_SYS_ADMIN_GROUPS));
        }
    }

    public void testRemoveUserFromGroupRemoveSysAdminGroupWithAnotherPresent()
            throws RemoteException
    {
        createGroup("sys-admin-group2");
        grantGlobalPermission(SYSTEM_ADMINISTER, "sys-admin-group2");
        addUserToGroup(ADMIN_USERNAME, "sys-admin-group2");

        userAdminSoapExerciser.testRemoveUserFromGroup(ADMINISTRATORS, ADMIN_USERNAME);

        gotoPage("/secure/admin/user/GroupBrowser.jspa");
        assertLinkPresentWithText(ADMINISTRATORS);
        clickLinkWithText(ADMINISTRATORS);
        clickLink("view_group_members");
        assertLinkNotPresent("deleteuser_link_" + ADMIN_USERNAME);
    }

    public void testRemoveUserFromGroupWithNoSysAdminPermRemoveLastAdmin()
    {
        try
        {
            restoreData("jira_soap_client_func_test_sys_admin.xml");
            soapConnect();
            userAdminSoapExerciser.testRemoveUserFromGroup(ADMINISTRATORS, ADMIN_USERNAME);
            fail("expected exception");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains(ERROR_LEAVING_ALL_ADMIN_GROUPS));
        }
        finally
        {
            logout();
            // go back to sysadmin user
            login("root", "root");
            restoreData("jira_soap_client_func_test_create_project.xml");
        }
    }

    public void testRemoveUserFromGroupWithNoSysAdminPermRemoveAdmin()
            throws RemoteException
    {
        try
        {
            restoreData("jira_soap_client_func_test_sys_admin.xml");

            createGroup("admin-group2");
            grantGlobalPermission(ADMINISTER, "admin-group2");
            addUserToGroup(ADMIN_USERNAME, "admin-group2");

            soapConnect();
            userAdminSoapExerciser.testRemoveUserFromGroup(ADMINISTRATORS, ADMIN_USERNAME);
            gotoPage("/secure/admin/user/GroupBrowser.jspa");
            assertLinkPresentWithText(ADMINISTRATORS);
            clickLinkWithText(ADMINISTRATORS);
            clickLink("view_group_members");
            assertLinkNotPresent("deleteuser_link_" + ADMIN_USERNAME);
        }
        finally
        {
            logout();
            // go back to sysadmin user
            login("root", "root");
            restoreData("jira_soap_client_func_test_create_project.xml");
        }
    }

    public void testRemoveUserFromGroupCanNotSeeGroup() throws RemoteException
    {
        try
        {
            restoreData("jira_soap_client_func_test_sys_admin.xml");

            soapConnect();
            userAdminSoapExerciser.testRemoveUserFromGroup("jira-sys-admins", SYS_ADMIN_USERNAME);
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You can not remove a group from this user as it is not visible to you."));
        }
        finally
        {
            logout();
            // go back to sysadmin user
            login("root", "root");
            restoreData("jira_soap_client_func_test_create_project.xml");
        }
    }

    public void testRemoveUserFromGroupCanNotLeaveNotAMemeber()
    {
        try
        {
            userAdminSoapExerciser.testRemoveUserFromGroup(ADMIN_GROUP, FRED_USERNAME);
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("Cannot remove user 'fred' from group 'jira-administrators' since user is not a member of 'jira-administrators'"));
        }
    }

    public void testAddUserToGroupCanNotSeeGroup() throws RemoteException
    {
        try
        {
            restoreData("jira_soap_client_func_test_sys_admin.xml");

            soapConnect();
            userAdminSoapExerciser.testAddUserToGroup("jira-sys-admins", ADMIN_USERNAME);
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You cannot add users to groups which are not visible to you."));
        }
        finally
        {
            logout();
            // go back to sysadmin user
            login("root", "root");
            restoreData("jira_soap_client_func_test_create_project.xml");
        }
    }

    public void testAddUserToGroup() throws RemoteException
    {
        userAdminSoapExerciser.testAddUserToGroup(GROUP_JIRA_DEVELOPERS, FRED_USERNAME);
        gotoPage("/secure/admin/user/GroupBrowser.jspa");
        assertLinkPresentWithText(GROUP_JIRA_DEVELOPERS);
        clickLinkWithText(GROUP_JIRA_DEVELOPERS);
        clickLink("view_group_members");
        assertTextPresent(FRED_USERNAME);
        assertLinkPresent("deleteuser_link_" + ADMIN_USERNAME);
        assertLinkPresent("deleteuser_link_" + FRED_USERNAME);
    }

    public void testAddUserToAdminGroup() throws RemoteException
    {
        userAdminSoapExerciser.testAddUserToGroup(ADMINISTRATORS, FRED_USERNAME);
        gotoPage("/secure/admin/user/GroupBrowser.jspa");
        assertLinkPresentWithText(ADMINISTRATORS);
        clickLinkWithText(ADMINISTRATORS);
        clickLink("view_group_members");
        assertTextPresent(FRED_USERNAME);
        assertLinkPresent("deleteuser_link_" + ADMIN_USERNAME);
        assertLinkPresent("deleteuser_link_" + FRED_USERNAME);
    }

    public void testAddUserToGroupUserDoesNotExist() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.testAddUserToGroup(ADMINISTRATORS, "invalid");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("Cannot add user. 'invalid' does not exist"));
        }
    }

    public void testAddUserToGroupUserAlreadyInGroup() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.testAddUserToGroup(ADMINISTRATORS, ADMIN_USERNAME);
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("Cannot add user 'admin', user is already a member of 'jira-administrators'"));
        }
    }

    public void testAddUserToGroupGroupDoesNotExist() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.testAddUserToGroup("invalid", ADMIN_USERNAME);
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("The group 'invalid' is not a valid group."));
        }
    }

    public void testRemoveUserFromGroupUserDoesNotExist() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.testRemoveUserFromGroup(ADMINISTRATORS, "invalid");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("Cannot remove user. 'invalid' does not exist"));
        }
    }

    public void testRemoveUserFromGroupGroupDoesNotExist() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.testRemoveUserFromGroup("invalid", ADMIN_USERNAME);
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("The group 'invalid' is not a valid group."));
        }
    }

    public void testRemoveUserFromAdminGroup() throws RemoteException
    {
        addUserToGroup(FRED_USERNAME, ADMINISTRATORS);
        userAdminSoapExerciser.testRemoveUserFromGroup(ADMINISTRATORS, FRED_USERNAME);
        gotoPage("/secure/admin/user/GroupBrowser.jspa");
        assertLinkPresentWithText(ADMINISTRATORS);
        clickLinkWithText(ADMINISTRATORS);
        clickLink("view_group_members");
        assertTextNotPresent(FRED_USERNAME);
        assertLinkPresent("deleteuser_link_" + ADMIN_USERNAME);
        assertLinkNotPresent("deleteuser_link_" + FRED_USERNAME);
    }

    public void testAddUserToGroupNoPerms() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.soapConnect(FRED_USERNAME, FRED_PASSWORD);
            userAdminSoapExerciser.testAddUserToGroup(GROUP_JIRA_DEVELOPERS, FRED_USERNAME);
            fail("Fred should not have permission to do anything.");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You must be at least a JIRA Administrator to manipulate a group."));
        }
    }

    public void testAddUserToGroupExternalUserManagement() throws RemoteException
    {
        try
        {
            toggleExternalUserManagement(true);
            userAdminSoapExerciser.testAddUserToGroup(GROUP_JIRA_DEVELOPERS, FRED_USERNAME);
            fail("External user management is enabled");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("Cannot edit group memberships, as external user management is enabled, please contact your JIRA administrators."));
        }
    }

    public void testRemoveUserFromGroupExternalUserManagement() throws RemoteException
    {
        try
        {
            toggleExternalUserManagement(true);
            userAdminSoapExerciser.testRemoveUserFromGroup(GROUP_JIRA_DEVELOPERS, FRED_USERNAME);
            fail("External user management is enabled");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("Cannot edit group memberships, as external user management is enabled, please contact your JIRA administrators."));
        }
    }

    public void testRemoveUserFromGroupNoPerms() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.soapConnect(FRED_USERNAME, FRED_PASSWORD);
            userAdminSoapExerciser.testRemoveUserFromGroup(GROUP_JIRA_DEVELOPERS, FRED_USERNAME);
            fail("Fred should not have permission to do anything.");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You must be at least a JIRA Administrator to manipulate a group."));
        }
    }

    public void testUpdateGroupNoPerms() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.soapConnect(FRED_USERNAME, FRED_PASSWORD);
            userAdminSoapExerciser.testUpdateGroup("jira-developers", new String[] { "fred" });
            fail("Fred should not have permission to do anything.");
        }
        catch (RemoteException e)
        {
            assertTrue(e.toString().contains("You must be at least a JIRA Administrator to manipulate a group."));
        }
    }

    public void testCreateGroup() throws RemoteException
    {
        userAdminSoapExerciser.testCreateGroup("jira-enthusiasts");
        gotoPage("/secure/admin/user/GroupBrowser.jspa");
        assertLinkPresentWithText("jira-enthusiasts");

        try
        {
            userAdminSoapExerciser.testCreateGroup(GROUP_JIRA_DEVELOPERS);
            fail("expected a RemoteValidationException");
        }
        catch (RemoteValidationException yay)
        {
            // expected
        }
    }

    public void testGetProjectRoles() throws RemoteException
    {
        RemoteProjectRole projectRole = userAdminSoapExerciser.testGetProjectRole();
        assertNotNull(projectRole);
        assertTrue(ExerciserClientConstants.USER_PROJECT_ROLE_NAME.equals(projectRole.getName()));

        RemoteProjectRole[] roles = userAdminSoapExerciser.testGetProjectRoles();
        assertNotNull(roles);

        // Assert that the 3 default roles exist
        assertEquals(3, roles.length);
    }

    public void testCreateProjectRole() throws RemoteException
    {
        RemoteProjectRole projectRole = userAdminSoapExerciser.testCreateRole("Test Role Name");
        assertNotNull(projectRole);

        gotoPage("secure/project/ViewProjectRoles.jspa");
        assertTextPresent("Test Role Name");

        // Test that a Remote Exception is thrown when using a duplicate name
        try
        {
            userAdminSoapExerciser.testCreateRole(ExerciserClientConstants.USER_PROJECT_ROLE_NAME);
            fail("A duplicate error should have been thrown for the role name");
        }
        catch (RemoteException e)
        {
        }
    }

    public void testIsRoleNameUnique() throws RemoteException
    {
        try
        {
            userAdminSoapExerciser.testIsRoleNameUnique(ExerciserClientConstants.USER_PROJECT_ROLE_NAME);
            fail("An exception should have been thrown");
        }
        catch (RemoteException e)
        {

        }

        boolean notUnique = userAdminSoapExerciser.testIsRoleNameUnique("Test Role Name Unique");
        assertTrue(notUnique);
    }

    public void testDeleteRole() throws RemoteException
    {
        RemoteProjectRole projectRole = getRemoteUserProjectRole();

        userAdminSoapExerciser.testDeleteRole(projectRole);
        gotoPage("secure/project/ViewProjectRoles.jspa");
        assertTextNotPresent("A role that represents users in a project");
    }

    public void testAddActorsToProjectRole() throws RemoteException
    {
        RemoteProjectRole userProjectRole = getRemoteUserProjectRole();
        RemoteProjectRole developerProjectRole = getRemoteDevelopersProjectRole();


        RemoteProject remoteProject = getRemoteproject();


        userAdminSoapExerciser.testAddActorsToProjectRole(developerProjectRole, remoteProject, FRED_USERNAME, ATLASSIAN_USER_ROLE_ACTOR);

        // Go to the Monkey project role page
        ProjectRoleClient prc = new ProjectRoleClient(environmentData);

        ProjectRole projectRole = prc.get(ExerciserClientConstants.MONKEY_PROJECT_KEY, "Developers");

        boolean found = false;
        for (ProjectRole.Actor actor : projectRole.actors)
        {
            if (actor.name.equals("fred"))
            {
                found = true;
            }
        }
        if (!found)
        {
            fail("Should have found fred");
        }

        userAdminSoapExerciser.testRemoveActorsFromProjectRole(userProjectRole, remoteProject, FRED_USERNAME, ATLASSIAN_USER_ROLE_ACTOR);

        // Since the XML data already has Fred assigned to the 'Users' we have to remove him from here as well.
        userAdminSoapExerciser.testRemoveActorsFromProjectRole(developerProjectRole, remoteProject, FRED_USERNAME, ATLASSIAN_USER_ROLE_ACTOR);

        // Go to the Monkey project role page
        projectRole = prc.get(ExerciserClientConstants.MONKEY_PROJECT_KEY, "Developers");

        for (ProjectRole.Actor actor : projectRole.actors)
        {
            if (actor.name.equals("fred"))
            {
                fail("Should not have found fred");
            }
        }

    }

    public void testUpdateRole() throws RemoteException
    {
        userAdminSoapExerciser.testUpdateRole(PROJECT_ID, UPDATED_DESCRIPTION);
        gotoPage("secure/project/ViewProjectRoles.jspa");
        assertTextPresent(UPDATED_DESCRIPTION);
    }

    public void testGetProjectRoleActors() throws RemoteException
    {

        RemoteProjectRoleActors actors = userAdminSoapExerciser.testGetProjectRoleActors(PROJECT_ID);

        assertNotNull(actors);
        assertTrue(actors.getUsers().length > 0);

    }

    public void testGetDefaultRoleActors() throws RemoteException
    {
        RemoteRoleActors actors = userAdminSoapExerciser.testGetDefaultRoleActors(PROJECT_ID);

        assertNotNull(actors);
        assertEquals(2, actors.getUsers().length);
    }

    public void testAddActorsToDefaultRole() throws RemoteException
    {
        RemoteProjectRole projectRole = getRemoteDevelopersProjectRole();

        userAdminSoapExerciser.testAddDefaultActorsToProjectRole(projectRole, FRED_USERNAME, ATLASSIAN_USER_ROLE_ACTOR);

        // Go to the default roles page
        gotoPage("secure/project/ViewProjectRoles.jspa?projectRoleId=" + ExerciserClientConstants.USER_PROJECT_ROLE_ID);
        clickLink("manage_" + ExerciserClientConstants.DEVELOPER_PROJECT_ROLE_NAME);
        assertTextPresent(FRED_FULLNAME);

        userAdminSoapExerciser.testRemoveDefaultActorsFromProjectRole(projectRole, FRED_USERNAME, ATLASSIAN_USER_ROLE_ACTOR);

        // Go to the default roles page
        gotoPage("secure/project/ViewProjectRoles.jspa?projectRoleId=" + ExerciserClientConstants.USER_PROJECT_ROLE_ID);
        clickLink("manage_" + ExerciserClientConstants.DEVELOPER_PROJECT_ROLE_NAME);
        assertTextNotPresent(FRED_FULLNAME);
    }

    public void testRemoveAllRoleActorsByNameAndType() throws RemoteException
    {
        RemoteProjectRole projectRole = getRemoteDevelopersProjectRole();

        RemoteProject remoteProject = getRemoteproject();

        userAdminSoapExerciser.testAddDefaultActorsToProjectRole(projectRole, FRED_USERNAME, ATLASSIAN_USER_ROLE_ACTOR);

        userAdminSoapExerciser.testAddActorsToProjectRole(projectRole, remoteProject, FRED_USERNAME, ATLASSIAN_USER_ROLE_ACTOR);

        userAdminSoapExerciser.testRemoveAllRoleActorsByNameAndType(FRED_USERNAME, ATLASSIAN_USER_ROLE_ACTOR);

        gotoPage("secure/project/ViewProjectRoles.jspa?projectRoleId=10000");
        clickLink("manage_" + ExerciserClientConstants.DEVELOPER_PROJECT_ROLE_NAME);
        assertTextNotPresent(FRED_FULLNAME);

        // Go to the Monkey project role page
        ProjectRoleClient prc = new ProjectRoleClient(environmentData);
        ProjectRole restProjectRole = prc.get(ExerciserClientConstants.MONKEY_PROJECT_KEY, "Developers");

        for (ProjectRole.Actor actor : restProjectRole.actors)
        {
            if (actor.name.equals("fred"))
            {
                fail("Should not have found fred");
            }
        }
    }

    public void testRemoveAllRoleActorsByProject() throws RemoteException
    {
        RemoteProject remoteProject = getRemoteproject();

        RemoteProjectRole projectRole = getRemoteDevelopersProjectRole();

        userAdminSoapExerciser.testAddActorsToProjectRole(projectRole, remoteProject, FRED_USERNAME, ATLASSIAN_USER_ROLE_ACTOR);

        userAdminSoapExerciser.testRemoveAllRoleActorsByProject(remoteProject);

        // Go to the Monkey project role page
        ProjectRoleClient prc = new ProjectRoleClient(environmentData);
        ProjectRole restProjectRole = prc.get(remoteProject.getKey(), "Developers");

        for (ProjectRole.Actor actor : restProjectRole.actors)
        {
            if (actor.name.equals("fred"))
            {
                fail("Should not have found fred");
            }
            if (actor.name.equals(GROUP_JIRA_USERS))
            {
                fail("Should not have found " + GROUP_JIRA_USERS);
            }

        }
    }


    public void testGetAssociatedNotificationSchemes() throws RemoteException
    {
        RemoteProjectRole projectRole = getRemoteDevelopersProjectRole();

        RemoteScheme[] remoteSchemes = projectAdminSoapExerciser.testGetAssociatedNotificationSchemes(projectRole);

        assertNotNull(remoteSchemes);

        assertEquals(1, remoteSchemes.length);

        assertEquals("Default Notification Scheme", remoteSchemes[0].getName());
    }

    public void testGetAssociatedPermissionsSchemes() throws RemoteException
    {
        RemoteProjectRole projectRole = getRemoteDevelopersProjectRole();

        RemoteScheme[] remoteSchemes = projectAdminSoapExerciser.testGetAssociatedPermissionSchemes(projectRole);

        assertNotNull(remoteSchemes);

        assertEquals(1, remoteSchemes.length);

        assertEquals("Default Permission Scheme", remoteSchemes[0].getName());
    }

    private RemoteProject getRemoteproject()
    {
        RemoteProject remoteProject = new RemoteProject();
        remoteProject.setId(ExerciserClientConstants.MONKEY_PROJECT_ID);
        remoteProject.setName(ExerciserClientConstants.MONKEY_PROJECT_NAME);
        remoteProject.setKey(ExerciserClientConstants.MONKEY_PROJECT_KEY);
        return remoteProject;
    }

    private RemoteProjectRole getRemoteUserProjectRole()
    {
        RemoteProjectRole projectRole = new RemoteProjectRole();
        projectRole.setId(new Long(ExerciserClientConstants.USER_PROJECT_ROLE_ID));
        projectRole.setName(ExerciserClientConstants.USER_PROJECT_ROLE_NAME);
        return projectRole;
    }

    private RemoteProjectRole getRemoteDevelopersProjectRole()
    {
        RemoteProjectRole projectRole = new RemoteProjectRole();
        projectRole.setId(new Long(ExerciserClientConstants.DEVELOPER_PROJECT_ROLE_ID));
        projectRole.setName(ExerciserClientConstants.DEVELOPER_PROJECT_ROLE_NAME);
        return projectRole;
    }
}