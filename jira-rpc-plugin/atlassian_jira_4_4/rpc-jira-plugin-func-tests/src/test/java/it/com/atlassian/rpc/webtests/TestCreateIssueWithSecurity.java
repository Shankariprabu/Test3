package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteField;
import com.atlassian.jira.rpc.soap.client.RemoteIssue;
import com.atlassian.jira.rpc.soap.client.RemoteSecurityLevel;
import com.atlassian.jira.rpc.soap.client.RemoteValidationException;
import com.atlassian.jira_soapclient.exercise.ExerciserClientConstants;

import java.rmi.RemoteException;

public class TestCreateIssueWithSecurity extends SOAPTestCase
{
    private static final String FAULT_STRING_LEVEL_REQUIRED = "com.atlassian.jira.rpc.exception.RemoteValidationException: {security=Security Level is required.} : []";
    private static final String ISSUE_TYPE_TODO = "6";
    private static final String A_SUBTASK_VIA_SOAP_SUMMARY = "A subtask?  Via SOAP?";

    public void testCreateIssueWithSecurity() throws RemoteException
    {
        restoreThisData("jira_soap_client_func_test_create_issue_with_security.xml");
        soapConnect();

        RemoteIssue issue = new RemoteIssue();
        issue.setProject("MKY");
        issue.setType(ExerciserClientConstants.ISSUE_TYPE_ID);
        issue.setSummary(ExerciserClientConstants.SUMMARY_NAME);
        issue.setPriority(ExerciserClientConstants.PRIORITY_ID);
        try
        {
            final RemoteIssue returnedIissue = issueSoapExerciser.testCreateIssueWithSecurity(issue, 10000);
            assertEquals("MKY", returnedIissue.getProject());
            assertEquals(ExerciserClientConstants.ISSUE_TYPE_ID, returnedIissue.getType());
            assertEquals(ExerciserClientConstants.SUMMARY_NAME, returnedIissue.getSummary());
            assertEquals(ExerciserClientConstants.PRIORITY_ID, returnedIissue.getPriority());
            gotoIssue("MKY-2");
            assertTextPresent("Everybody");
            final RemoteSecurityLevel remoteSecurityLevel = soapSession.getJiraSoapService().getSecurityLevel(getToken(), "MKY-2");
            assertEquals("Everybody", remoteSecurityLevel.getName());
            assertEquals("10000", remoteSecurityLevel.getId());
            assertEquals("Short desc", remoteSecurityLevel.getDescription());
        }
        catch (RemoteException e)
        {
            fail();
        }

    }

    public void testCreateIssueWithNoSecurity() throws RemoteException
    {
        restoreThisData("jira_soap_client_func_test_create_issue_with_security.xml");
        soapConnect();

        RemoteIssue issue = new RemoteIssue();
        issue.setProject("MKY");
        issue.setType(ExerciserClientConstants.ISSUE_TYPE_ID);
        issue.setSummary(ExerciserClientConstants.SUMMARY_NAME);
        issue.setPriority(ExerciserClientConstants.PRIORITY_ID);
        try
        {
            issueSoapExerciser.testCreateIssue(issue);
            fail("Should have thrown an exception!");
        }
        catch (RemoteValidationException e)
        {
            assertMessageContains(e, FAULT_STRING_LEVEL_REQUIRED);
        }
    }

    public void testCreateIssueWithInvalidSecurityLevel() throws RemoteException
    {
        restoreThisData("jira_soap_client_func_test_create_issue_with_security.xml");
        soapConnect();

        RemoteIssue issue = new RemoteIssue();
        issue.setProject("MKY");
        issue.setType(ExerciserClientConstants.ISSUE_TYPE_ID);
        issue.setSummary(ExerciserClientConstants.SUMMARY_NAME);
        issue.setPriority(ExerciserClientConstants.PRIORITY_ID);
        try
        {
            issueSoapExerciser.testCreateIssueWithSecurity(issue, 999);
            fail();
        }
        catch (RemoteException e)
        {
            // expected
            assertMessageContains(e, "security=The security level is invalid.");
        }
    }

    public void testCreateIssueWithSecurityLevelWhenNotRequired() throws RemoteException
    {
        restoreThisData("jira_soap_client_func_test_create_issue_with_security.xml");
        soapConnect();

        RemoteIssue issue = new RemoteIssue();
        issue.setProject(ExerciserClientConstants.PROJECT_KEY);
        issue.setType(ExerciserClientConstants.ISSUE_TYPE_ID);
        issue.setSummary(ExerciserClientConstants.SUMMARY_NAME);
        issue.setPriority(ExerciserClientConstants.PRIORITY_ID);
        try
        {
            issueSoapExerciser.testCreateIssueWithSecurity(issue, 10000);
            fail();
        }
        catch (RemoteException e)
        {
            // expected
            assertMessageContains(e, "This user does not have the 'set issue security' permission.");
        }
    }

    public void testFieldsForCreate() throws RemoteException
    {
        restoreThisData("jira_soap_client_func_test_create_subtaskissue_with_security.xml");
        soapConnect();

        try
        {
            RemoteField[] remoteFields = issueSoapExerciser.testFieldsForCreate("HSP", 1L);
            assertNotNull(remoteFields);

            assertContainsFields(remoteFields,
                    "summary", "priority", "duedate",
                    "components", "versions", "fixVersions",
                    "assignee", "reporter", "environment",
                    "description", "timetracking", "labels",
                    "customfield_10000", "customfield_10001", "customfield_10010");
        }
        catch (RemoteException e)
        {
            fail(e.toString());
        }
    }

    private void assertContainsFields(RemoteField[] remoteFields, String... fieldIds)
    {
        for (String fieldId : fieldIds)
        {
            boolean found = false;
            for (RemoteField remoteField : remoteFields)
            {
                if (remoteField.getId().equals(fieldId))
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                fail("This remote fields did not contain expected field id : '" + fieldId + "'");
            }
        }
    }

    public void testCreateSubTaskIssue() throws RemoteException
    {
        restoreThisData("jira_soap_client_func_test_create_subtaskissue_with_security.xml");
        soapConnect();

        administration.subtasks().disable();

        RemoteIssue issue = createSubTaskRemoteIssue("HSP");
        try
        {
            issueSoapExerciser.testCreateIssueWithParent(issue, "HSP-1");
            fail("should have failed because subtasks are off");
        }
        catch (RemoteException e)
        {
            assertMessageContains(e, "Subtasks are not enabled in this instance of JIRA");
        }
        administration.subtasks().enable();

        issue = createSubTaskRemoteIssue("HSP");
        try
        {
            final RemoteIssue returnedIissue = issueSoapExerciser.testCreateIssueWithParent(issue, "HSP-1");
            assertEquals("HSP", returnedIissue.getProject());
            assertEquals(ISSUE_TYPE_TODO, returnedIissue.getType());
            assertEquals(A_SUBTASK_VIA_SOAP_SUMMARY, returnedIissue.getSummary());
            assertEquals(ExerciserClientConstants.PRIORITY_ID, returnedIissue.getPriority());
        }
        catch (RemoteException e)
        {
            fail(e.toString());
        }

        assertBadParent(issue, "BOLLOCKS-1", "This issue does not exist or you don't have permission to view it");
        assertBadParent(issue, "TST-1", "The parent issue is not in the same project");
    }

    private RemoteIssue createSubTaskRemoteIssue(final String project)
    {
        RemoteIssue issue = new RemoteIssue();
        issue.setProject(project);
        issue.setType(ISSUE_TYPE_TODO); // matches our t-odo class
        issue.setSummary(A_SUBTASK_VIA_SOAP_SUMMARY);
        issue.setPriority(ExerciserClientConstants.PRIORITY_ID);
        return issue;
    }

    private void assertBadParent(final RemoteIssue issue, final String parentKey, final String expectedMessageContains)
    {
        // ok with the same data lets test a bad parent
        try
        {
            issueSoapExerciser.testCreateIssueWithParent(issue, parentKey);
            fail("Should have barfed with parental problems");
        }
        catch (RemoteException expected)
        {
            assertMessageContains(expected, expectedMessageContains);
        }
    }

    private void assertMessageContains(final RemoteException e, final String expectedContains)
    {
        final String actualMessage = e.toString();
        assertTrue("Looking for " + expectedContains + " but found : " + actualMessage, actualMessage.contains(expectedContains));
    }

    private void restoreThisData(final String dataFile)
    {
        administration.restoreData(dataFile);
    }
}
