package it.com.atlassian.rpc.webtests;

import com.atlassian.jira.rpc.soap.client.RemoteComment;
import com.atlassian.jira.rpc.soap.client.RemoteComponent;
import com.atlassian.jira.rpc.soap.client.RemoteCustomFieldValue;
import com.atlassian.jira.rpc.soap.client.RemoteField;
import com.atlassian.jira.rpc.soap.client.RemoteIssue;
import com.atlassian.jira.rpc.soap.client.RemoteVersion;

import java.rmi.RemoteException;
import java.util.Calendar;

public class TestIssueServiceSOAPClient extends SOAPTestCase
{
    public TestIssueServiceSOAPClient(String s)
    {
        super(s);
    }

    public void setUp()
    {
        super.setUp();
        restoreData("TestIssueServiceSOAPClient.xml");
        try
        {
            soapConnect();
        }
        catch (RemoteException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Test to ensure that JRA-13703 is fixed.  A user with full permissions should be able to see all editable
     * fields, whereas a user with limited permissions should only see a subset of those fields.
     *
     * @throws RemoteException
     */
    public void testGetFieldForEdit() throws RemoteException
    {

        String token = getToken();
        RemoteField[] remoteFields = soapSession.getJiraSoapService().getFieldsForEdit(token, "HSP-1");
        assertNotNull(remoteFields);
        assertEquals(15, remoteFields.length);
        assertEquals("summary", remoteFields[0].getId());
        assertEquals("issuetype", remoteFields[1].getId());
        assertEquals("priority", remoteFields[2].getId());
        assertEquals("duedate", remoteFields[3].getId());
        assertEquals("components", remoteFields[4].getId());
        assertEquals("versions", remoteFields[5].getId());
        assertEquals("fixVersions", remoteFields[6].getId());
        assertEquals("assignee", remoteFields[7].getId());
        assertEquals("reporter", remoteFields[8].getId());
        assertEquals("environment", remoteFields[9].getId());
        assertEquals("description", remoteFields[10].getId());
        assertEquals("customfield_10000", remoteFields[11].getId());
        assertEquals("customfield_10001", remoteFields[12].getId());
        assertEquals("customfield_10010", remoteFields[13].getId());
        assertEquals("labels", remoteFields[14].getId());

        //now login as a user who doesn't have access to some fields.
        token = soapSession.getJiraSoapService().login("fred", "fred");
        remoteFields = soapSession.getJiraSoapService().getFieldsForEdit(token, "HSP-1");
        assertNotNull(remoteFields);
        assertEquals(11, remoteFields.length);
        assertEquals("summary", remoteFields[0].getId());
        assertEquals("issuetype", remoteFields[1].getId());
        assertEquals("priority", remoteFields[2].getId());
        //duedate is protected
        assertEquals("components", remoteFields[3].getId());
        assertEquals("versions", remoteFields[4].getId());
        //fixversions, assignee & reporter is protected
        assertEquals("environment", remoteFields[5].getId());
        assertEquals("description", remoteFields[6].getId());
        assertEquals("customfield_10000", remoteFields[7].getId());
        assertEquals("customfield_10001", remoteFields[8].getId());
        assertEquals("customfield_10010", remoteFields[9].getId());
        assertEquals("labels", remoteFields[10].getId());
    }

    public void testGetIssue() throws RemoteException
    {
        final String token = getToken();
        RemoteIssue issue = soapSession.getJiraSoapService().getIssue(token, "HSP-1");
        assertIssue(issue);
    }

    public void testGetIssueById() throws RemoteException
    {
        final String token = getToken();
        RemoteIssue issue = soapSession.getJiraSoapService().getIssueById(token, "10000");
        assertIssue(issue);
    }

    public void testGetIssueWithDeletedAssignee() throws Exception
    {
        final String token = getToken();
        RemoteIssue issue = soapSession.getJiraSoapService().getIssue(token, "MKY-1");
        assertEquals("MKY-1", issue.getKey());
        // The user "Barney Rubble" was deleted, so the remote issue should just have the username in it.
        assertEquals("barney", issue.getAssignee());
    }

    public void testGetIssueWithDeletedReporter() throws Exception
    {
        final String token = getToken();
        RemoteIssue issue = soapSession.getJiraSoapService().getIssue(token, "HSP-2");
        assertEquals("HSP-2", issue.getKey());
        // The user "Barney Rubble" was deleted, so the remote issue should just have the username in it.
        assertEquals("barney", issue.getReporter());
    }

    public void testGetCommentsWithDeletedUser() throws Exception
    {
        final String token = getToken();
        RemoteComment[] comments = soapSession.getJiraSoapService().getComments(token, "MKY-1");
        assertEquals(1, comments.length);
        assertEquals("barney", comments[0].getAuthor());
    }

    private void assertCustomFieldValues(RemoteIssue issue)
    {
        RemoteCustomFieldValue[] customFieldValues = issue.getCustomFieldValues();
        assertNotNull(customFieldValues);
        assertEquals(2, customFieldValues.length);
        for (RemoteCustomFieldValue customFieldValue : customFieldValues)
        {
            if ("customfield_10000".equals(customFieldValue.getCustomfieldId()))
            {
                assertEquals("custom field 1 value", customFieldValue.getValues()[0]);
            }
            else if ("customfield_10001".equals(customFieldValue.getCustomfieldId()))
            {
                assertEquals("custom field 2 value", customFieldValue.getValues()[0]);
            }
        }
    }

    private void assertIssue(RemoteIssue issue)
    {
        assertNotNull(issue);
        assertEquals("HSP-1", issue.getKey());
        assertEquals("admin", issue.getAssignee());
        RemoteVersion[] affectsVersions = issue.getAffectsVersions();
        assertEquals(0, affectsVersions.length);
        String[] attachmentNames = issue.getAttachmentNames();
        assertEquals(0, attachmentNames.length);
        RemoteComponent[] remoteComponents = issue.getComponents();
        assertEquals(0, remoteComponents.length);
        final Calendar created = Calendar.getInstance();
        created.set(2006, 4, 1, 16, 21, 54);
        //ignore millis
        created.set(Calendar.MILLISECOND, 0);
        issue.getCreated().set(Calendar.MILLISECOND, 0);
        assertEquals(created.getTimeInMillis(), issue.getCreated().getTimeInMillis());
        assertCustomFieldValues(issue);
        assertNull(issue.getDescription());
        assertNull(issue.getDuedate());
        assertNull("HSP-1", issue.getEnvironment());
        RemoteVersion[] versions = issue.getFixVersions();
        assertEquals(0, versions.length);
        assertEquals("10000", issue.getId());
        assertEquals("3", issue.getPriority());
        assertEquals("HSP", issue.getProject());
        assertEquals("admin", issue.getReporter());
        assertNull(issue.getResolution());
        assertEquals("1", issue.getStatus());
        assertEquals("Test bug 1", issue.getSummary());
        assertEquals("1", issue.getType());
        final Calendar updated = Calendar.getInstance();
        updated.set(2006, 4, 1, 17, 22, 40);
        updated.set(Calendar.MILLISECOND, 0);
        issue.getUpdated().set(Calendar.MILLISECOND, 0);
        assertEquals(updated.getTimeInMillis(), issue.getUpdated().getTimeInMillis());
        assertEquals(new Long(0), issue.getVotes());
    }

}